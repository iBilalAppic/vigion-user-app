package com.vpsappall.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Bilal Khan on 25/8/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static int DATABASE_VERSION = 1;

    public static String DATABASENAME = "Driver.db";
    public static String TABLE_NAME = "LocationTable";
    public static String DIRECTION_TABLE_NAME = "DirectionsTable";

    public static String LATITUDE = "LATITUDE";
    public static String DirectionData = "DirectionData";
    public static String LONGITUDE = "LONGITUDE";
    public static String ENTRY_DT = "ENTRY_DT";

    private static String CREATE_TABLE;
    private static String CREATE_DIRECTIONS_TABLE;
    SQLiteDatabase db;
    private String TRACKING_ID ="TrackingId";

    private static DBHelper dbH = null;

    public static DBHelper getInstance(Context ctx) {

        if (dbH == null) {
            dbH = new DBHelper(ctx.getApplicationContext());
        }
        return dbH;
    }

    public DBHelper(Context context) {
        super(context, DATABASENAME, null, DATABASE_VERSION);
        CREATE_TABLE = "create table LocationTable(id integer primary key autoincrement,LATITUDE text,LONGITUDE text , ENTRY_DT text)";
        CREATE_DIRECTIONS_TABLE = "create table DirectionsTable(id integer primary key autoincrement,DirectionData text,TrackingId text )";

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_DIRECTIONS_TABLE);
        Log.e("Table Created", "Table Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS LocationTable");
        db.execSQL("DROP TABLE IF EXISTS DirectionsTable");
        onCreate(db);
//
//        if (newVersion > oldVersion) {
//            db.execSQL("ALTER TABLE "+DIRECTION_TABLE_NAME+" ADD COLUMN "+ DRIVER_NAME +" TEXT DEFAULT ''");
//        }

    }

    public boolean insert(String Latitude, String Longitude, String Entry_dt) {

        db = this.getWritableDatabase();
        ContentValues init = new ContentValues();
        init.put(LATITUDE, Latitude);
        init.put(LONGITUDE, Longitude);
        init.put(ENTRY_DT, Entry_dt);

        db.insert(TABLE_NAME, null, init);

        Log.e("", "");
        
        return true;
    }

    public boolean insertDirections(String Name,String directions) {

        db = this.getWritableDatabase();
        ContentValues init = new ContentValues();
        init.put(DirectionData, directions);
        init.put(TRACKING_ID, Name);
        db.insert(DIRECTION_TABLE_NAME, null, init);
        Log.e("DIRECTIONS_TABLE", directions);
        

        return true;
    }

    public String getDirections(String driverName) {

        db = this.getReadableDatabase();

//        String sql="SELECT * FROM "+DIRECTION_TABLE_NAME+" ORDER BY ROWID ASC LIMIT 1";

        String sql = "select " + DirectionData + " from " + DIRECTION_TABLE_NAME + " WHERE "+ TRACKING_ID +" = '"+driverName+ "'";
        Log.i("sql",sql);
        Cursor mCur = db.rawQuery(sql, null);
        mCur.moveToFirst();

        String directions = mCur.getString(mCur.getColumnIndex(DirectionData));
        Log.i("directions", directions);
        


        return directions;

    }

    public String getAllDirectionTableData(){


        db = this.getReadableDatabase();

//        String sql="SELECT * FROM "+DIRECTION_TABLE_NAME+" ORDER BY ROWID ASC LIMIT 1";

        String sql = "select * from " + DIRECTION_TABLE_NAME ;
        Cursor mCur = db.rawQuery(sql, null);
        mCur.moveToFirst();

        String directions = mCur.getString(mCur.getColumnIndex(DirectionData));
        String driverName = mCur.getString(mCur.getColumnIndex(TRACKING_ID));
        Log.i("driverName", driverName);
        Log.i("directions", directions);


        return directions;
    }

    public boolean checkIfValueExists(String driverName)
    {
        String query = "SELECT "+ TRACKING_ID +" FROM  "+DIRECTION_TABLE_NAME+"  WHERE + "+ TRACKING_ID +"  =?";
        Cursor cursor = db.rawQuery(query, new String[]{driverName});

        if (cursor.getCount() > 0)
        {
            
            return true;
        }
        else
            
        return false;
    }

    public long getNumOfRows(){

        db=this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, DIRECTION_TABLE_NAME);
        
        return count;
    }

    public void deleteTabData(){
        db=this.getWritableDatabase();
        db.execSQL("delete from "+ DIRECTION_TABLE_NAME);
        

    }

    public String[]  getAllColumnName(){
        db = getReadableDatabase();

        Cursor dbCursor = db.query(DIRECTION_TABLE_NAME, null, null, null, null, null, null);
        String[] columnNames = dbCursor.getColumnNames();

        return columnNames;
    }

    public boolean isTableEmpty(String table) {
        db=this.getWritableDatabase();
        String count = "SELECT count(*) FROM "+table;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0){
            return false;
        } else{
        return true;
        }

    }

    public void updateTableRowEmpty(String Name,String data){
        db=this.getWritableDatabase();
        db.execSQL("UPDATE "+DIRECTION_TABLE_NAME+" SET "+DirectionData+"='"+data+"'  WHERE "+ TRACKING_ID +" = "+Name+"");
        

    }

    public void deleteDriver(String Name){
        db=this.getWritableDatabase();
//        db.execSQL("delete from "+DIRECTION_TABLE_NAME+" where "+DRIVER_NAME+"='"+Name+"'");
        db.delete(DIRECTION_TABLE_NAME, ""+ TRACKING_ID +"=?", new String[]{Name});

        Log.i("deleted",Name);
    }

}
