package com.vpsappall.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.vpsappall.Adapter.HistoryAdapter;
import com.vpsappall.Model.HistoryModel;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    Context context;
    RecyclerView historyRecyc;
    TextView noDataHistory;
    LinearLayoutManager layoutManager;
    HistoryAdapter historyAdapter;

    AppPreferance appPreferance;
    ArrayList<HistoryModel> arrayList = new ArrayList<>();

    String userId, loginType;

    public HistoryFragment() {
        // Required empty public constructor

        context = getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_history, container, false);

        appPreferance = new AppPreferance();
        userId = appPreferance.getPreferences(getContext(), AppConstant.uniqueId);
        loginType = appPreferance.getPreferences(getContext(), AppConstant.LoginType);

        noDataHistory = (TextView) view.findViewById(R.id.noDataHistory);
        historyRecyc = (RecyclerView) view.findViewById(R.id.historyRecyc);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        historyRecyc.setLayoutManager(layoutManager);

        FetchHistoryList();

        return view;
    }

    private void FetchHistoryList() {


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", userId);
            jsonObject.put("type", loginType);
            Log.e("Histrory Req", jsonObject.toString());


            RequestGenerator.makePostRequest(getActivity(), AppConstant.ListOfHistory, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(context, "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        Log.e("History Res", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");
                        if (status.equals("success")) {
                            JSONArray jsonArray = jsonObject1.getJSONArray("trackhistory");
                            if (loginType.toString().equals("customer")) {
                                arrayList.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    HistoryModel historyModel = new HistoryModel();
                                    historyModel.setTrackingId(jsonObject2.getString("trackingid"));
                                    historyModel.setDriverName(jsonObject2.getString("drivername"));
                                    historyModel.setDriverPic(jsonObject2.getString("driverpic"));
                                    historyModel.setVehicleNumber(jsonObject2.getString("vehiclenumber"));
                                    historyModel.setPackageName(jsonObject2.getString("packagename"));


                                    arrayList.add(historyModel);
                                }
                            } else if (loginType.toString().equals("transporter") || loginType.toString().equals("client")) {
                                arrayList.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    HistoryModel historyModel = new HistoryModel();
                                    historyModel.setTrackingId(jsonObject2.getString("trackingid"));
                                    historyModel.setDriverName(jsonObject2.getString("drivername"));
                                    historyModel.setDriverPic(jsonObject2.getString("driverpic"));
                                    historyModel.setVehicleNumber(jsonObject2.getString("vehiclenumber"));
                                    historyModel.setRouteType(jsonObject2.getString("routetype"));


                                    arrayList.add(historyModel);
                                }
                            }
                            noDataHistory.setVisibility(View.GONE);
                            historyAdapter = new HistoryAdapter(getActivity(), arrayList);
                            historyRecyc.setAdapter(historyAdapter);

                        } else {
                            noDataHistory.setVisibility(View.VISIBLE);
                            historyRecyc.setVisibility(View.GONE);
                            noDataHistory.setText(message);
                        }

                    }

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
