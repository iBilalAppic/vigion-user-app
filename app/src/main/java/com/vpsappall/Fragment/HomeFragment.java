package com.vpsappall.Fragment;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {


    private Context mContext;
    private FrameLayout mLive, mHistory;
    private TextView liveTitle, historyTitle, mLiveView, mHistoryView;

    private static HomeFragment instance;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        instance = HomeFragment.this;
        getActivity().setTitle(R.string.app_name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mLive = (FrameLayout) view.findViewById(R.id.tab_live);
        mHistory = (FrameLayout) view.findViewById(R.id.tab_history);

        liveTitle = (TextView) view.findViewById(R.id.liveTitle);
        historyTitle = (TextView) view.findViewById(R.id.historyTitle);
        mLiveView = (TextView) view.findViewById(R.id.liveView);
        mHistoryView = (TextView) view.findViewById(R.id.historyView);


        setupViewPager();

        return view;
    }


    private void setupViewPager() {
        mLive.setOnClickListener(this);
        mHistory.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tab_live:
                UtilsClass.goToFragment(mContext, new HomeLiveFragment(), R.id.home_tab_container, false);
                mLiveView.setVisibility(View.VISIBLE);
                mHistoryView.setVisibility(View.GONE);
                liveTitle.setTextColor(Color.parseColor("#000000"));
                historyTitle.setTextColor(Color.parseColor("#BCBCBC"));
                break;

            case R.id.tab_history:
                UtilsClass.goToFragment(mContext, new HistoryFragment(), R.id.home_tab_container, false);
                mLiveView.setVisibility(View.GONE);
                mHistoryView.setVisibility(View.VISIBLE);
                liveTitle.setTextColor(Color.parseColor("#BCBCBC"));
                historyTitle.setTextColor(Color.parseColor("#000000"));
                break;
        }
    }

    public static HomeFragment getInstance() {
        return instance;
    }
}
