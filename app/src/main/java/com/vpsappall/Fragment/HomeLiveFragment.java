package com.vpsappall.Fragment;


import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.vpsappall.Activity.LiveTrackingOnMapActivity;
import com.vpsappall.Database.DBHelper;
import com.vpsappall.Model.TrackListModel;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeLiveFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback,
        LocationListener {
    private static HomeLiveFragment instance;
    private static final String TAG = HomeLiveFragment.class.getSimpleName();
    MapView mMapView;
    private GoogleMap googleMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    AppPreferance appPreferance;
    String userId, packageNa, driverNa, vehivleNo, driverPic;
    ArrayList<TrackListModel> arrayListCust = new ArrayList<>();
    ArrayList<String> markerPlaces = new ArrayList<>();

    public HomeLiveFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = HomeLiveFragment.this;
        appPreferance = new AppPreferance();
        userId = appPreferance.getPreferences(getActivity(), AppConstant.uniqueId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//

        DBHelper dbHelper=DBHelper.getInstance(getActivity().getApplicationContext());
        Log.i("direRows",""+dbHelper.getNumOfRows());
        Log.i("direRows",""+ Arrays.toString(dbHelper.getAllColumnName()));

        SQLiteDatabase database=DBHelper.getInstance(getActivity().getApplicationContext()).getWritableDatabase();
        Log.i("version",database.getVersion()+"");

        //        Log.i("direRows",""+dbHelper.getNumOfRows());

        View view = inflater.inflate(R.layout.fragment_home_live, container, false);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.setVisibility(View.GONE);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();

        }

        mMapView.getMapAsync(this);
        initUI();
        return view;
    }


    public void initUI() {
        FetchTrackingList();
    }

    private void FetchTrackingList() {


        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", userId);

            RequestGenerator.makePostRequest(getActivity(), AppConstant.TrackingList, jsonObject,
                    true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getContext(), "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        Log.e("Response:", jsonObject1.toString());
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");
                        if (status.equals("success")) {
                            JSONArray jsonArray = jsonObject1.getJSONArray("tracking");
                            arrayListCust.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                TrackListModel trackListModel = new TrackListModel();
                                trackListModel.setTrackingId(jsonObject2.getString("trackingid"));
                                trackListModel.setDriverName(jsonObject2.getString("drivername"));
                                trackListModel.setDriverPic(jsonObject2.getString("driverpic"));
                                trackListModel.setVehicleNumber(jsonObject2.getString("vehiclenumber"));
                                trackListModel.setPackageName(jsonObject2.getString("packagename"));
                                trackListModel.setLatitude(jsonObject2.getString("latitude"));
                                trackListModel.setLongitude(jsonObject2.getString("longitude"));

                                arrayListCust.add(trackListModel);

                            }

                            checkLocationPermission();
                            onMapReady(googleMap);
                            googleMap.setMyLocationEnabled(false);

                        } else {

                            UtilsClass.showToast(getContext(), message);

                            googleMap.setMyLocationEnabled(true);
                        }
                    }
                }

            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int hour = cal.get(Calendar.HOUR_OF_DAY); //Get the hour from the calendar
        try {
            boolean success;
            if (hour <= 17 && hour >= 6) {
                success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.style_json_light));

            } else {

                success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.style_json));
            }
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }


        mMapView.setVisibility(View.VISIBLE);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (int i = 0; i < arrayListCust.size(); i++) {
            final TrackListModel trackListModel = arrayListCust.get(i);

            Double truckLat = Double.valueOf(trackListModel.getLatitude());
            Double truckLong = Double.valueOf(trackListModel.getLongitude());

            packageNa = trackListModel.getPackageName();
            driverNa = trackListModel.getDriverName();
            vehivleNo = trackListModel.getVehicleNumber();
            driverPic = trackListModel.getDriverPic();

            LatLng latLng = new LatLng(truckLat, truckLong);
            final Location location = new Location(latLng.toString());
            location.setLatitude(truckLat);
            location.setLongitude(truckLong);
            int height = 40;
            int width = 40;
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dot);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);


            final MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(trackListModel.getPackageName());
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap((smallMarker)));
            Marker marker = mMap.addMarker(markerOptions);
            markerPlaces.add(marker.getId());

            builder.include(marker.getPosition());
            animateMarker(location, marker);
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//            mMap.animateCamera(CameraUpdateFactory.zoomTo(13));

            LatLngBounds bounds = builder.build();
            int width1 = getResources().getDisplayMetrics().widthPixels;
            int height1 = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.25);

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width1, height1, padding);
            mMap.animateCamera(cu);

        }

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View v = getActivity().getLayoutInflater().inflate(R.layout.windowlayout, null);
                render(marker, v);

                return v;

            }

            @Override
            public View getInfoContents(Marker marker) {
//                View v = getActivity().getLayoutInflater().inflate(R.layout.windowlayout, null);
//                render(marker, v);
//                return v;
                return null;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (markerPlaces.contains(marker.getId())) {
                    int positionnn = markerPlaces.indexOf(marker.getId());
                    Intent intent = new Intent(getActivity(), LiveTrackingOnMapActivity.class);
                    intent.putExtra("trackingId", arrayListCust.get(positionnn).getTrackingId());
                    intent.putExtra("latn", arrayListCust.get(positionnn).getLatitude());
                    intent.putExtra("lang", arrayListCust.get(positionnn).getLongitude());
                    intent.putExtra("DriverName", arrayListCust.get(positionnn).getDriverName());
                    startActivity(intent);
                } else {
                    UtilsClass.showToast(getActivity(), "No Tracking Id Found");
                }
            }
        });

    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        initUI();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        googleMap.setMyLocationEnabled(true);
                    }
                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

    }


    private void render(Marker marker, View v) {

        if (marker != null) {
            if (markerPlaces.contains(marker.getId())) {
                int positionnn = markerPlaces.indexOf(marker.getId());

                CircleImageView diverImg = (CircleImageView) v.findViewById(R.id.historyPic);
                LatLng latLng = marker.getPosition();
                marker.setPosition(latLng);
                TextView tvLat = (TextView) v.findViewById(R.id.tv_lat);
                TextView driverName = (TextView) v.findViewById(R.id.driverName);
                TextView vehx = (TextView) v.findViewById(R.id.vehicleNumber);

                final CircleImageView showRoute = (CircleImageView) v.findViewById(R.id.user_Route);
                if(arrayListCust.get(positionnn).getDriverPic()!=null) {
                    Picasso.with(getActivity()).load(arrayListCust.get(positionnn).getDriverPic())
                            .placeholder(R.drawable.avatar).into(diverImg);

                }


                tvLat.setText(arrayListCust.get(positionnn).getPackageName());
                driverName.setText(arrayListCust.get(positionnn).getDriverName());
                vehx.setText(arrayListCust.get(positionnn).getVehicleNumber());

            } else {
            }
        }

    }


    public static void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000); // duration 1 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        marker.setRotation(computeRotation(v, startRotation, destination.getBearing()));
                    } catch (Exception ex) {
                        // I don't care atm..
                    }
                }
            });

            valueAnimator.start();
        }
    }

    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }


    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }

    }

    public static HomeLiveFragment getInstance() {
        return instance;
    }


}