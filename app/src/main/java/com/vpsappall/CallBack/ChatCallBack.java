package com.vpsappall.CallBack;


import com.vpsappall.Interface.ChatInterface;

/**
 * Created by android on 02/08/17.
 */

public class ChatCallBack {
    ChatInterface chatInterface;

    public ChatCallBack(ChatInterface chatInterface) {
        this.chatInterface = chatInterface;
    }

    public void calledFromMain(String jsonstring, boolean refresh) {
        //Do somthing...

        //call back main
        chatInterface.callback(jsonstring, refresh);
    }

}
