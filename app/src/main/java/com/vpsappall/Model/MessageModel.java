package com.vpsappall.Model;

/**
 * Created by Bilal Khan on 2/8/17.
 */

public class MessageModel {
    private String senderId;
    private String receiverId;
    private String message;
    private String date;

    private String username;
    private String profilepic;
    private String type;
    private String sendingvisivility;

    public String getSendingvisivility() {
        return sendingvisivility;
    }

    public void setSendingvisivility(String sendingvisivility) {
        this.sendingvisivility = sendingvisivility;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
