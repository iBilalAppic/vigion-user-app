package com.vpsappall.Model;

/**
 * Created by Bilal Khan on 1/8/17.
 */

public class LiveTruckModel {

    String trackingId;
    String drivername;
    String driverpic;
    String vehiclenumber;
    String destination;
    String latitude;
    String longitude;

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getDriverpic() {
        return driverpic;
    }

    public void setDriverpic(String driverpic) {
        this.driverpic = driverpic;
    }

    public String getVehiclenumber() {
        return vehiclenumber;
    }

    public void setVehiclenumber(String vehiclenumber) {
        this.vehiclenumber = vehiclenumber;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
