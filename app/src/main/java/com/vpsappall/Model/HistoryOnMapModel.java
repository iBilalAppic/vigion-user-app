package com.vpsappall.Model;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 21/6/17.
 */

public class HistoryOnMapModel {

    String trackingId;
    ArrayList<NavigationData> data;

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public ArrayList<NavigationData> getData() {
        return data;
    }

    public void setData(ArrayList<NavigationData> data) {
        this.data = data;
    }

    public class NavigationData {

        String latitude;
        String longitude;

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }
    }

}
