package com.vpsappall.Model;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 23/6/17.
 */

public class LiveTrackingModel {

    String trackingId;
    String driverName;
    String vehiclenumber;
    String packagename;
    String latitude;
    String longitude;
    String desLatitude;
    String desLongitude;
    String endrequest;
    ArrayList<NavigationData> data;


    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getVehiclenumber() {
        return vehiclenumber;
    }

    public void setVehiclenumber(String vehiclenumber) {
        this.vehiclenumber = vehiclenumber;
    }

    public String getPackagename() {
        return packagename;
    }

    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDesLatitude() {
        return desLatitude;
    }

    public void setDesLatitude(String desLatitude) {
        this.desLatitude = desLatitude;
    }

    public String getDesLongitude() {
        return desLongitude;
    }

    public void setDesLongitude(String desLongitude) {
        this.desLongitude = desLongitude;
    }

    public String getEndrequest() {
        return endrequest;
    }

    public void setEndrequest(String endrequest) {
        this.endrequest = endrequest;
    }

    public ArrayList<NavigationData> getData() {
        return data;
    }

    public void setData(ArrayList<NavigationData> data) {
        this.data = data;
    }

    public class NavigationData {

        String latitude;
        String longitude;

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }
    }
}
