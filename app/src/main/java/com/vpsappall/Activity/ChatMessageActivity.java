package com.vpsappall.Activity;

import android.content.Context;
import android.os.Handler;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.vpsappall.Adapter.ChatMessageAdapter;
import com.vpsappall.CallBack.ChatCallBack;
import com.vpsappall.ChatService.ChatWebservice;
import com.vpsappall.ChatService.SendMessageWebservice;
import com.vpsappall.Interface.ChatInterface;
import com.vpsappall.Model.MessageModel;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.InternetChecking;
import com.bumptech.glide.Glide;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;
import com.vpsappall.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ChatMessageActivity extends AppCompatActivity implements View.OnClickListener, ChatInterface {

    String type, trackingId, driverName, driverPic, userId, driverId, loginType;

    LinearLayout EditSendLL;
    private RecyclerView mRecyclerView;
    private ImageView ivSend, ivProfile;
    private EditText mEditTextMessage;
    private String receiverId;
    private ChatMessageAdapter mAdapter;
    private ArrayList<MessageModel> messages = new ArrayList<>();
    private ImageView ivUserPic;
    private TextView tvName;
    private Handler myHandler = new Handler();
    private AppPreferance mAppPreferences;
    private int chatSize = 0;
    private InternetChecking internetChecking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_message);

        EditSendLL = (LinearLayout) findViewById(R.id.send_message_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ivSend = (ImageView) findViewById(R.id.ivSend);
        mEditTextMessage = (EditText) findViewById(R.id.et_message);
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        tvName = (TextView) findViewById(R.id.tvName);
        findViewById(R.id.ivBack).setOnClickListener(this);
        ivProfile.setOnClickListener(this);
        mAppPreferences = new AppPreferance();


        userId = mAppPreferences.getPreferences(getApplicationContext(), AppConstant.uniqueId);
        loginType = mAppPreferences.getPreferences(getApplicationContext(), AppConstant.LoginType);

        if (!userId.equals("")) {
            type = getIntent().getStringExtra("type");
            trackingId = getIntent().getStringExtra("trackingid");
            driverName = getIntent().getStringExtra("driverName");
            driverPic = getIntent().getStringExtra("driverPic");
            driverId = getIntent().getStringExtra("driverId");
        }

        internetChecking = new InternetChecking();
        tvName.setText(driverName);
        try {
            Glide.with(ChatMessageActivity.this).load(driverPic).centerCrop().into(ivProfile);
        } catch (Exception e) {
            e.printStackTrace();
            ivProfile.setImageResource(R.drawable.ic_avatar_dark);
        }
        LinearLayoutManager manager = new LinearLayoutManager(ChatMessageActivity.this);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new ChatMessageAdapter(ChatMessageActivity.this, messages);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);


        hideKeyboard();

        if (internetChecking.checkConnection(ChatMessageActivity.this)) {
            displayChatHistoryMessage(false, true);
            seenScreenofChat();

        } else {
            Toast.makeText(ChatMessageActivity.this, "No internet connection...!", Toast.LENGTH_SHORT).show();
        }


        myHandler.postDelayed(UpdateData, 1000);

        if (type.equals("history")) { //
            EditSendLL.setVisibility(View.GONE);
        }


        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mEditTextMessage.getText().toString();
                if (TextUtils.isEmpty(message)) {
                    return;
                }

                if (internetChecking.checkConnection(ChatMessageActivity.this)) {
                    SendMessage(message);
                    mEditTextMessage.setText("");

                } else {
                    Toast.makeText(ChatMessageActivity.this, "No internet connection...!", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    public Runnable UpdateData = new Runnable() {
        public void run() {
            try {
                displayChatHistoryMessage(true, false);
                myHandler.postDelayed(this, 1000);
            } catch (Exception e) {
            }
        }
    };

    private void SendMessage(String message) {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("trackingid", trackingId);
            jsonObject.put("userid", userId);
            jsonObject.put("message", message);

            Log.d("Mes Req", jsonObject.toString());


            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                String formattedDate = df.format(c.getTime());

                MessageModel addmsg = new MessageModel();
                addmsg.setUsername(mAppPreferences.getPreferences(ChatMessageActivity.this, AppConstant.username));
                addmsg.setMessage(message);
                addmsg.setDate("" + formattedDate);
                addmsg.setReceiverId("");
                addmsg.setSenderId(userId);
                addmsg.setType("receiver");
                addmsg.setSendingvisivility("true");
                mAdapter.addsendmsg(addmsg);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
            } catch (Exception e) {
                e.printStackTrace();
            }

            ChatCallBack chatCallBack = new ChatCallBack((ChatInterface) ChatMessageActivity.this);
            new SendMessageWebservice(this, jsonObject, chatCallBack).execute();

        } catch (Exception e) {

        }

    }

    @Override
    public void callback(String jsonstring, boolean refresh) {
        Log.v("JsonString", jsonstring);
        if (!jsonstring.equals("") && !jsonstring.isEmpty()) {

            try {
                messages.clear();
                JSONObject jsonObject = new JSONObject(jsonstring);

                String status = jsonObject.getString("status");
                if (status.equals("success")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("chats");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        MessageModel messageModel = new MessageModel();
                        messageModel.setSenderId(jsonObject1.getString("senderid"));
                        messageModel.setReceiverId(jsonObject1.getString("receiverid"));
                        messageModel.setMessage(jsonObject1.getString("message"));
                        messageModel.setDate(jsonObject1.getString("createdate"));

                        JSONObject jsonObject2 = jsonObject1.getJSONObject("receiver");

                        messageModel.setUsername(jsonObject2.getString("username"));
                        messageModel.setProfilepic(jsonObject2.getString("profilepic"));
                        messageModel.setType(jsonObject2.getString("type"));

                        messages.add(messageModel);

                    }
                }

                if (refresh) {
                    //messages.addAll(messageResponse.getMessage());
                    if (mAdapter != null) {

                        if (chatSize < messages.size()) {
                            chatSize = messages.size();
                            mAdapter.addItemsToList(messages);
                            mAdapter.notifyDataSetChanged();
                            mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                        }
                    } else {
                        if (!(messages == null)) {
                            mAdapter = new ChatMessageAdapter(ChatMessageActivity.this, messages);
                            mRecyclerView.setAdapter(mAdapter);
                            mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                        }
                    }
                } else {
                    messages.clear();
                    messages.addAll(messages);
                    if (messages.size() > 0) {
                        chatSize = messages.size();
                        mAdapter = new ChatMessageAdapter(ChatMessageActivity.this, messages);
                        mRecyclerView.setAdapter(mAdapter);
                        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void displayChatHistoryMessage(final boolean isRefresh, final boolean isFirstRun) {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", userId);
            jsonObject.put("trackingid", trackingId);
            jsonObject.put("driverid", driverId);

            ChatCallBack chatCallBack = new ChatCallBack(ChatMessageActivity.this);
            new ChatWebservice(this, jsonObject, chatCallBack, isRefresh, isFirstRun).execute();


        } catch (Exception e) {

        }

    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        myHandler.removeCallbacks(UpdateData);
    }


    private void seenScreenofChat() {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", userId);
            jsonObject.put("trackingid", trackingId);

            RequestGenerator.makePostRequest(ChatMessageActivity.this, AppConstant.ChatSeenStatus, jsonObject, false, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {

                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");

                        if (status.equals("success")) {
                            Log.e("success1", message);
                        } else {
                            Log.e("success2", message);
                        }
                    }
                }
            });
        } catch (Exception e) {

        }
    }
}
