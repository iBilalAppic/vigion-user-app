package com.vpsappall.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.vpsappall.Adapter.NotificationAdapter;
import com.vpsappall.Model.NotificationModel;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {

    private static NotificationActivity instance;
    Context context;
    RecyclerView recyclerView;
    TextView noData;
    LinearLayoutManager layoutManager;
    NotificationAdapter notificationAdapter;
    AppPreferance appPreferance;
    String userId, loginType;


    ArrayList<NotificationModel> arrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        instance = NotificationActivity.this;
        context = NotificationActivity.this;
        UtilsClass.fullScreen(context);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNoti);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        appPreferance = new AppPreferance();
        userId = appPreferance.getPreferences(NotificationActivity.this, AppConstant.uniqueId);
        loginType = appPreferance.getPreferences(NotificationActivity.this, AppConstant.LoginType);
        initUI();


    }

    public void initUI() {
        init();
    }

    private void init() {

        noData = (TextView) findViewById(R.id.noNoti);
        recyclerView = (RecyclerView) findViewById(R.id.noti_recycler);

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        FetchNotificationData();

    }

    private void FetchNotificationData() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", userId);
            jsonObject.put("type", loginType);

            Log.e("Noti Req", jsonObject.toString());

            RequestGenerator.makePostRequest(NotificationActivity.this, AppConstant.ListOfNotifications, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(NotificationActivity.this, "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        Log.e("Noti Res", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");

                        if (status.equals("success")) {
                            JSONArray jsonArray = jsonObject1.getJSONArray("notification");
                            arrayList.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                NotificationModel notificationModel = new NotificationModel();
                                notificationModel.setNotificationId(jsonObject2.getString("notificationid"));
                                notificationModel.setTrackingId(jsonObject2.getString("trackingid"));
                                notificationModel.setDriverName(jsonObject2.getString("drivername"));
                                notificationModel.setDriverPic(jsonObject2.getString("driverpic"));
                                notificationModel.setVehicleNumber(jsonObject2.getString("vehiclenumber"));
                                notificationModel.setPackageName(jsonObject2.optString("packagename"));
                                notificationModel.setMessage(jsonObject2.getString("message"));
                                notificationModel.setType(jsonObject2.getString("type"));
                                notificationModel.setDate(jsonObject2.getString("date"));
                                arrayList.add(notificationModel);

                            }

                            noData.setVisibility(View.GONE);
                            notificationAdapter = new NotificationAdapter(NotificationActivity.this, arrayList);
                            recyclerView.setAdapter(notificationAdapter);


                        } else {
                            noData.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            noData.setText(message);
                        }

                    }
                }
            });

        } catch (JSONException e) {
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(NotificationActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static NotificationActivity getInstance() {
        return instance;
    }

}
