package com.vpsappall.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.VolleyError;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;
import com.rey.material.widget.Switch;

import org.json.JSONException;
import org.json.JSONObject;

public class SettingActivity extends AppCompatActivity {

    LinearLayout soundLL, changePassLL, editLL;
    com.rey.material.widget.Switch notiSwitch;
    TextView soundName;

    AppPreferance appPreferance;
    String userId, loginType, notiPref, notiRing, unid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSetting);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        appPreferance = new AppPreferance();
        notiPref = appPreferance.getPreferences(getApplicationContext(), AppConstant.getNoti);
        Log.e("Noti", notiPref);
        notiRing = appPreferance.getPreferences(getApplicationContext(), AppConstant.defaultRingtone);
        userId = appPreferance.getPreferences(SettingActivity.this, AppConstant.uniqueId);
        unid = appPreferance.getPreferences(SettingActivity.this, AppConstant.userId);
        loginType = appPreferance.getPreferences(SettingActivity.this, AppConstant.LoginType);

        init();

    }

    private void init() {

        editLL = (LinearLayout) findViewById(R.id.editProfileLL);
        changePassLL = (LinearLayout) findViewById(R.id.changePassLL);
        soundLL = (LinearLayout) findViewById(R.id.soundLL);
        notiSwitch = (com.rey.material.widget.Switch) findViewById(R.id.pushNotifiSwitch);
        soundName = (TextView) findViewById(R.id.soundName);

        if (notiPref.toString().equals("")) {
            appPreferance.setPreferences(getApplicationContext(), AppConstant.getNoti, "1");
        }
        if (notiPref.toString().equals("1")) {
            notiSwitch.setChecked(true);
        } else {
            notiSwitch.setChecked(false);
        }
        editLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, EditProfile.class);
                startActivity(intent);
                finish();
            }
        });
        soundLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                notiRing = appPreferance.getPreferences(getApplicationContext(), AppConstant.defaultRingtone);

                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Notification Sound");
                if (notiRing.toString().equals("")) {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
                } else {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(notiRing));
                }
                startActivityForResult(intent, 5);
            }
        });


        notiSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch view, boolean checked) {
                if (checked == true) {
                    appPreferance.setPreferences(SettingActivity.this, AppConstant.getNoti, "1");
                    Toast.makeText(SettingActivity.this, "Notification On", Toast.LENGTH_SHORT).show();
                } else {
                    appPreferance.setPreferences(SettingActivity.this, AppConstant.getNoti, "0");
                    Toast.makeText(SettingActivity.this, "Notification Off", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (appPreferance.getPreferences(getApplicationContext(), AppConstant.defaultRingtone).equals("")) {
            soundName.setText("Default");
        } else {
            soundName.setText(appPreferance.getPreferences(SettingActivity.this, AppConstant.ringtoneName));
        }
        changePassLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeDialog();
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        if (resultCode == Activity.RESULT_OK && requestCode == 5) {
            Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

            if (uri != null) {
                Ringtone ringtone = RingtoneManager.getRingtone(SettingActivity.this, uri);
                String ringtoneName = ringtone.getTitle(SettingActivity.this);
                appPreferance.setPreferences(SettingActivity.this, AppConstant.defaultRingtone, uri.toString());
                appPreferance.setPreferences(SettingActivity.this, AppConstant.ringtoneName, ringtoneName);
//                Toast.makeText(SettingActivity.this, "" + ringtoneName, Toast.LENGTH_SHORT).show();
//                this.chosenRingtone = uri.toString();
                soundName.setText(ringtoneName);
//                RingtoneManager.setActualDefaultRingtoneUri(SettingActivity.this, RingtoneManager.TYPE_NOTIFICATION, uri);

            } else {
//                this.chosenRingtone = null;
            }
        }
    }

   /* public ArrayList<String> getNotificationSounds() {
        RingtoneManager manager = new RingtoneManager(this);
        manager.setType(RingtoneManager.TYPE_NOTIFICATION);
        Cursor cursor = manager.getCursor();

        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            String id = cursor.getString(RingtoneManager.ID_COLUMN_INDEX);
            String uri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);

            list.add(uri + "/" + id);
            Log.e("List", uri + " / " + id);
        }

        return list;
    }*/

    private void showChangeDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.change_password_ui, null);

        final EditText et_old_password, et_new_password;

        et_old_password = (EditText) view.findViewById(R.id.et_old_password);
        et_new_password = (EditText) view.findViewById(R.id.et_new_password);

        builder.setView(view);
        builder.setTitle("Change Password");
        builder.setPositiveButton("Change Password", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String old_password = et_old_password.getText().toString();
                String new_password = et_new_password.getText().toString();
                if (old_password.isEmpty()) {
                    et_old_password.setError("Old Password Required");
                } else if (new_password.isEmpty()) {
                    et_new_password.setError("New Password Required");
                } else {

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("userid", unid);
                        jsonObject.put("oldpassword", old_password);
                        jsonObject.put("newpassword", new_password);
                        jsonObject.put("type", loginType);

                        RequestGenerator.makePostRequest(SettingActivity.this, AppConstant.ChangePassword, jsonObject, true, new ResponseListener() {
                            @Override
                            public void onError(VolleyError error) {
                                UtilsClass.showToast(SettingActivity.this, "Network Issue Occured");
                            }

                            @Override
                            public void onSuccess(String string) throws JSONException {
                                if (!string.equals("")) {
                                    JSONObject jsonObject1 = new JSONObject(string);
                                    String status = jsonObject1.getString("status");
                                    String message = jsonObject1.getString("message");
                                    if (status.equals("success")) {
                                        dialog.dismiss();
                                        UtilsClass.showToast(SettingActivity.this, message);
                                    } else {
                                        UtilsClass.showToast(SettingActivity.this, message);
                                    }
                                }
                            }
                        });

                    } catch (JSONException e) {
                    }
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SettingActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
