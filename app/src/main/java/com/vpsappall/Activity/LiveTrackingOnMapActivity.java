package com.vpsappall.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vpsappall.Database.DBHelper;
import com.vpsappall.Model.LiveTrackingModel;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.DataParser;
import com.vpsappall.OtherClass.MarkerAnimation;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;
import com.vpsappall.Services.TrackingService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.vpsappall.R.id.map;

public class LiveTrackingOnMapActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private static final String TAG = LiveTrackingOnMapActivity.class.getSimpleName();
    ArrayList<LatLng> pointspoints = new ArrayList<>();

    ImageButton imageButton;
    private static LiveTrackingOnMapActivity instance;
    private GoogleMap mMap;
    ArrayList<LiveTrackingModel> arrayList = new ArrayList<>();
    String trackingId, newTrackignId;
    SupportMapFragment mapFragment;
    TextView chatCount;
    Marker marker;
    private FloatingActionButton fab;
    boolean firstcheck = false;
    private boolean isMarkerRotating = false;
    AppPreferance appPreferance;
    private ArrayList<LatLng> points = new ArrayList<LatLng>();
    String paths, userGuide, userGuideLive;
    //    PolylineOptions options;
    private String latn, lang, userId, chatCountString, loginType;
    private Polyline polyline;
    private TapTargetSequence tapTargetSequence;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_tracking_on_map);
        dbHelper = DBHelper.getInstance(getApplicationContext());


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
        trackingId = getIntent().getStringExtra("trackingId");
        latn = getIntent().getStringExtra("latn");
        lang = getIntent().getStringExtra("lang");

        fab = (FloatingActionButton) findViewById(R.id.fab_btn);
        chatCount = (TextView) findViewById(R.id.chatCount);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LiveTrackingOnMapActivity.this, DetailsActivity.class);
                intent.putExtra("trackingId", newTrackignId);
                intent.putExtra("details", "live");
                startActivity(intent);
            }
        });
        appPreferance = new AppPreferance();
        userId = appPreferance.getPreferences(LiveTrackingOnMapActivity.this, AppConstant.uniqueId);
        loginType = appPreferance.getPreferences(LiveTrackingOnMapActivity.this, AppConstant.LoginType);
        userGuide = appPreferance.getPreferences(LiveTrackingOnMapActivity.this, AppConstant.USER_GUIDE_FLAG);
        userGuideLive = appPreferance.getPreferences(LiveTrackingOnMapActivity.this, AppConstant.USER_GUIDE_FOR_LIVE_TRACK);
        imageButton = (ImageButton) findViewById(R.id.backImg);
        imageButton.setOnClickListener(this);

        startTrackingService();
        if (userGuideLive.equals("1")) {
            final Display display = getWindowManager().getDefaultDisplay();
            final Drawable droid = getResources().getDrawable(R.drawable.ic_dot);
            final Rect droidTarget = new Rect(0, 0, droid.getIntrinsicWidth() * 2, droid.getIntrinsicHeight() * 2);
            droidTarget.offset(display.getWidth() / 2, display.getHeight() / 2);
            tapTargetSequence = new TapTargetSequence(this)
                    .targets(
                            TapTarget.forView(findViewById(R.id.backImg), "This is Back Button", "Click here to go back."),
                            TapTarget.forView(findViewById(R.id.fab_btn), "More Detail Button", "Click here to see More Details Of Tracking  .")

                                    .dimColor(R.color.black)
                                    .outerCircleColor(R.color.colorPrimary)
                                    .targetCircleColor(R.color.white)
                                    .textColor(R.color.white)
                    )
                    .listener(new TapTargetSequence.Listener() {
                        // This listener will tell us when interesting(tm) events happen in regards
                        // to the sequence
                        @Override
                        public void onSequenceFinish() {
                            // Yay
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        }


                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {
                            // Boo
                        }
                    });
            tapTargetSequence.continueOnCancel(true);
            tapTargetSequence.start();
            appPreferance.setPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FOR_LIVE_TRACK, "0");

        }
    }

    private void startTrackingService() {
        this.startService(new Intent(this, TrackingService.class));
    }

    private void stopTrackingService() {
        this.stopService(new Intent(this, TrackingService.class));
    }


    public void showLiveRoute() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("trackingid", trackingId);
            jsonObject.put("userid", userId);
            jsonObject.put("type", loginType);

            Log.e("LiveReq", jsonObject.toString());
            RequestGenerator.makePostRequest(LiveTrackingOnMapActivity.this, AppConstant.LiveTracking,
                    jsonObject, false, new ResponseListener() {
                        @Override
                        public void onError(VolleyError error) {
                            UtilsClass.showToast(getApplicationContext(), "Some Problem Occured");
                        }

                        @Override
                        public void onSuccess(String string) throws JSONException {
                            if (!string.equals("")) {
                                Log.e("LiveResp", string);
                                JSONObject jsonObject1 = new JSONObject(string);
                                String status = jsonObject1.getString("status");
                                String message = jsonObject1.getString("message");
                                chatCountString = jsonObject1.getString("chatcount");
                                if (status.equals("success")) {
                                    JSONArray jsonArray = jsonObject1.getJSONArray("livetracking");
                                    arrayList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                        LiveTrackingModel liveTrackingModel = new LiveTrackingModel();
                                        liveTrackingModel.setTrackingId(jsonObject2.getString("trackingid"));
                                        liveTrackingModel.setDriverName(jsonObject2.getString("drivername"));
                                        liveTrackingModel.setVehiclenumber(jsonObject2.getString("vehiclenumber"));
                                        liveTrackingModel.setPackagename(jsonObject2.getString("packagename"));
                                        liveTrackingModel.setLatitude(jsonObject2.getString("latitude"));
                                        liveTrackingModel.setLongitude(jsonObject2.getString("longitude"));
                                        liveTrackingModel.setEndrequest(jsonObject2.getString("endrequest"));
                                        liveTrackingModel.setDesLatitude(jsonObject2.getString("destlatitude"));
                                        liveTrackingModel.setDesLongitude(jsonObject2.getString("destlongitude"));
                                        JSONArray jsonArray1 = jsonObject2.getJSONArray("navigation");
//                                latn = jsonObject2.getString("latitude");
//                                lang = jsonObject2.getString("longitude");
                                        newTrackignId = jsonObject2.getString("trackingid");
                                        ArrayList<LiveTrackingModel.NavigationData> navigationDatas = new ArrayList<LiveTrackingModel.NavigationData>();
                                        for (int j = 0; j < jsonArray1.length(); j++) {
                                            JSONObject jsonObject3 = jsonArray1.getJSONObject(j);

                                            LiveTrackingModel.NavigationData navigationData = liveTrackingModel.new NavigationData();

                                            navigationData.setLongitude(jsonObject3.getString("longitude"));
                                            navigationData.setLatitude(jsonObject3.getString("latitude"));
                                            navigationDatas.add(navigationData);

                                        }
                                        liveTrackingModel.setData(navigationDatas);

                                        arrayList.add(liveTrackingModel);
                                    }
                                    if (chatCountString.toString().equals("0")) {
                                        chatCount.setVisibility(View.INVISIBLE);
                                    } else {
                                        chatCount.setVisibility(View.VISIBLE);
                                        chatCount.setText(chatCountString);
                                    }
                                    loadMap();


                                } else {

                                    stopTrackingService();
                                }
                            }

                        }
                    });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int hour = cal.get(Calendar.HOUR_OF_DAY);
//        mMap.setTrafficEnabled(true);
        try {
            boolean success;
            if (hour <= 17 && hour >= 6) {
                success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json_light));

            } else {

                success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
            }
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        if (!trackingId.equals("")) {
            showLiveRoute();
            if (lang != null || latn != null) {
                LatLng latLngFirst = new LatLng(Double.parseDouble(latn), Double.parseDouble(lang));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngFirst, 14));
            }
        }
    }

    public void loadMap() {
        ArrayList<LiveTrackingModel.NavigationData> arrayList12 = new ArrayList<>();
        ArrayList<List<LiveTrackingModel.NavigationData>> partitions = new ArrayList<>();

        LatLng LatLangdest = null;
        LatLng latLngCurrent = null;
        if (firstcheck) {
            Log.e("firstcheck", "false");
            for (int i = 0; i < arrayList.size(); i++) {
                Double latitudeDes = Double.valueOf(arrayList.get(i).getDesLatitude());
                Double longitudeDes = Double.valueOf(arrayList.get(i).getDesLongitude());
                LatLangdest = new LatLng(latitudeDes, longitudeDes);
                latLngCurrent = new LatLng(Double.valueOf(arrayList.get(i).getLatitude()), Double.valueOf(arrayList.get(i).getLongitude()));

            }

//            MarkerOptions markerOptions1 = new MarkerOptions().position(latLngCurrent).title("New Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//            mMap.addMarker(markerOptions1);

            try {
                if (!latLngCurrent.equals("")) {

                    String url = getDirectionsUrl(marker.getPosition(), latLngCurrent);


                    Log.e("Url Called", url);
                    FetchUrl1 FetchUrl1 = new FetchUrl1();
                    FetchUrl1.execute(url);


//                    snapLiveRoad(marker.getPosition(), latLngCurrent);
                    final Location location = new Location(latLngCurrent.toString());
                    location.setLatitude(latLngCurrent.latitude);
                    location.setLongitude(latLngCurrent.longitude);

                } else {
                    UtilsClass.showToast(getApplicationContext(), "No Current Location Found");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            float bearingg = (float) bearingBetweenLocations(marker.getPosition(), latLngCurrent);
////            rotateMarker(marker, bearingg);
            rotateMarker(marker, latLngCurrent, bearingg);


        }
        else {
            Log.e("firstcheck", "true");
            firstcheck = true;
            StringBuilder s = new StringBuilder();
            LatLng latLngStart = null;
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList12 = arrayList.get(i).getData();


                Double latitudeDes = Double.valueOf(arrayList.get(i).getDesLatitude());
                Double longitudeDes = Double.valueOf(arrayList.get(i).getDesLongitude());
                LatLangdest = new LatLng(latitudeDes, longitudeDes);
                latLngCurrent = new LatLng(Double.valueOf(arrayList.get(i).getLatitude()), Double.valueOf(arrayList.get(i).getLongitude()));

                LatLng latLng = null;


                if (arrayList12.size() <= 0) {


//                    options.add(latLngCurrent);
                    MarkerOptions markerOptionsDes = new MarkerOptions().position(LatLangdest)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)).title("Destination");
                    mMap.addMarker(markerOptionsDes);
//                    mMap.addPolyline(options);
//                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngCurrent));
//                    mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
//                    BitmapDescriptor icon1 = BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_new_truck_one);
//                    MarkerOptions markerOptions1 = new MarkerOptions().position(latLngCurrent).icon(icon1).title("Current Location").flat(true).anchor(0.5f, 0.5f);
//                    marker = mMap.addMarker(markerOptions1);
                    int height = 40;
                    int width = 40;
                    BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dot);
                    Bitmap b = bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                    MarkerOptions markerOptions1 = new MarkerOptions().position(latLngCurrent).title("Current Location")
                            .icon(BitmapDescriptorFactory.fromBitmap((smallMarker))).flat(true).anchor(0.5f, 0.5f);
//                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngCurrent, 16));
                    marker = mMap.addMarker(markerOptions1);
                } else {

                    for (int j = 0; j < arrayList12.size(); j++) {

                        if (!arrayList12.isEmpty()) {

                            Double lat = Double.valueOf(arrayList12.get(j).getLatitude());
                            Double lang = Double.valueOf(arrayList12.get(j).getLongitude());
                            latLng = new LatLng(lat, lang);
                            points.add(latLng);
//                            if (j == 0) {
//
//                            } else {p
//
//                                int apos = j - 1;
//                                int bpos = j;
//                                latLngAPos = new LatLng(Double.valueOf(arrayList12.get(apos).getLatitude()), Double.valueOf(arrayList12.get(apos).getLongitude()));
//                                latLngBPos = new LatLng(Double.valueOf(arrayList12.get(bpos).getLatitude()), Double.valueOf(arrayList12.get(bpos).getLongitude()));
//                                String url = getDirectionsUrl(latLngAPos, latLngBPos);
//                                FetchUrl1 FetchUrl1 = new FetchUrl1();
//                                FetchUrl1.execute(url);
//                            }

                            latLngStart = new LatLng(Double.valueOf(arrayList12.get(0).getLatitude()),
                                    Double.valueOf(arrayList12.get(0).getLongitude()));

                            MarkerOptions markerOptions = new MarkerOptions().position(latLngStart).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).title("Start");
                            mMap.addMarker(markerOptions);
//                            options.add(latLng);
                        }


                        MarkerOptions markerOptionsDes = new MarkerOptions().position(LatLangdest)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                                .title("Destination");
                        mMap.addMarker(markerOptionsDes);
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                    }


//                    BitmapDescriptor icon1 = BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_new_truck_one);
//                    MarkerOptions markerOptions1 = new MarkerOptions().position(latLngCurrent).icon(icon1).title("Current Location").flat(true).anchor(0.5f, 0.5f);
//                    marker = mMap.addMarker(markerOptions1);
                    int height = 40;
                    int width = 40;
                    BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dot);
                    Bitmap b = bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                    MarkerOptions markerOptions1 = new MarkerOptions().position(latLngCurrent).title("Current Location").icon(BitmapDescriptorFactory.fromBitmap((smallMarker))).flat(true).anchor(0.5f, 0.5f);
//                    mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
                    marker = mMap.addMarker(markerOptions1);

                }

            }

            if (points.size() > 99) {

                for (int k = 0; k < 100; k++) {
//                partitions.add(arrayList12.subList(k, Math.min(k + partitionSize, arrayList12.size())));

                    s.append(arrayList12.get(k).getLatitude());
                    s.append(",");
                    s.append(arrayList12.get(k).getLongitude());
                    s.append("|");


                }
                if (s.length() > 0) {
                    s.setLength(s.length() - 1);
                }
                paths = s.toString();
                callSnapToRoad();

                Log.e("Path", points.size() + " " + paths);

            } else {
                if (points.size() <= 0) {

                } else {
                    for (int k = 0; k < points.size(); k++) {
//                partitions.add(arrayList12.subList(k, Math.min(k + partitionSize, arrayList12.size())));

                        s.append(arrayList12.get(k).getLatitude());
                        s.append(",");
                        s.append(arrayList12.get(k).getLongitude());
                        s.append("|");


                    }
                    if (s.length() > 0) {
                        s.setLength(s.length() - 1);
                    }
                    paths = s.toString();
                    callSnapToRoad();

                    Log.e("Path", points.size() + " " + paths);
                }
            }
            try {
                if (!latLngCurrent.equals("")) {

                    animateMarker(latLngCurrent, false);

//                    animateMarker(mMap, marker, points);

                } else {

                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        Log.i("direction Count",dbHelper.getNumOfRows()+"");
        if (!dbHelper.isTableEmpty(DBHelper.DIRECTION_TABLE_NAME)) {


            if(dbHelper.checkIfValueExists(trackingId)) {

//            }if (!dbHelper.getDirections(DriverName).equals("")) {

                String result = dbHelper.getDirections(trackingId);
                ParserTask parserTask = new ParserTask();
             parserTask.execute(result);

            } else {
                String url = getDirectionsUrl(marker.getPosition(), latLngCurrent);
                Log.e("Url Called", url);
                FetchUrl FetchUrl = new FetchUrl();
                FetchUrl.execute(url);
            }

        } else {
            //to draw blue polyline
            String url = getDirectionsUrl(latLngCurrent, LatLangdest);
            FetchUrl FetchUrl = new FetchUrl();
            FetchUrl.execute(url);
        }


    }


    public void forLoopMethod() {
        StringBuilder s = new StringBuilder();


        if (points.size() > 99) {

            for (int k = 0; k < 100; k++) {
//                partitions.add(arrayList12.subList(k, Math.min(k + partitionSize, arrayList12.size())));

                s.append(points.get(k).latitude);
                s.append(",");
                s.append(points.get(k).longitude);
                s.append("|");


            }
            if (s.length() > 0) {
                s.setLength(s.length() - 1);
            }
            paths = s.toString();
            callSnapToRoad();

        } else {
            if (points.size() <= 0) {

            } else {
                for (int k = 0; k < points.size(); k++) {
//                partitions.add(arrayList12.subList(k, Math.min(k + partitionSize, arrayList12.size())));

                    s.append(points.get(k).latitude);
                    s.append(",");
                    s.append(points.get(k).longitude);
                    s.append("|");


                }
                if (s.length() > 0) {
                    s.setLength(s.length() - 1);
                }
                paths = s.toString();
                callSnapToRoad();
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void callSnapToRoad() {
        String urlA = "https://roads.googleapis.com/v1/snapToRoads?path=" + paths + "&interpolate=true&key=" +
                "AIzaSyBR3YZ_k07pFYBcEZPLYpDAZvO9NdtOSOU";
        Log.e("RequestSnap", urlA);
        RequestGenerator.makeGetRequest(LiveTrackingOnMapActivity.this, urlA, false, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Log.e("Failor", "response failor");
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {
                    Log.e("succes", "response " + string);
                    PolylineOptions lineOptions = new PolylineOptions();
                    ArrayList<LatLng> point = new ArrayList<LatLng>();
                    JSONObject jsonObject = new JSONObject(string);
                    JSONArray jsonArray = jsonObject.getJSONArray("snappedPoints");
                    LatLng latLng = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        JSONObject jsonObject2 = jsonObject1.getJSONObject("location");

                        double latitude = jsonObject2.getDouble("latitude");
                        double longitude = jsonObject2.getDouble("longitude");
                        latLng = new LatLng(latitude, longitude);
//                        lineOptions.add(latLng);

                        point.add(latLng);
                    }

                    lineOptions.addAll(point);
                    lineOptions.width(10);
                    lineOptions.color(getResources().getColor(R.color.dorange));

                    mMap.addPolyline(lineOptions);

                    double latitude = point.get(point.size() - 1).latitude;
                    double longitude = point.get(point.size() - 1).longitude;

                    latLng = new LatLng(latitude, longitude);
                    BitmapDescriptor icon1 = BitmapDescriptorFactory.fromResource(R.drawable.ic_dot);
                    MarkerOptions markerOptions1 = new MarkerOptions().position(latLng).icon(icon1).title("Driver")
                            .flat(true).anchor(0.5f, 0.5f);

                    mMap.addMarker(markerOptions1);

                    if (points.size() > 99) {
//                        for (int i = 0; i < 100; i++) {
//                            points.remove(i);
//
//                        }
                        points.subList(0, 99).clear();
                    } else {
                        if (points.size() <= 0) {

                        } else {
//                            for (int i = 0; i < 100; i++) {
//                                points.remove(points.size());
//                            }
                            points.subList(0, points.size()).clear();
                        }
                    }
                    forLoopMethod();

                } else {
                    Log.e("succes", "response Empty");
                }

            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == imageButton) {
            onBackPressed();
        }

    }


    public void animateMarker(final LatLng toPosition, final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();

        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1500;


        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;

                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
//                mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));

//                float bearingg = (float) bearingBetweenLocations(marker.getPosition(), toPosition);
//                rotateMarker(marker, bearingg);

                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });


    }


    private void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
//                        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
//                                .target(newPosition)
//                                .zoom(15.5f)
//                                .build()));

                        marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));
                    } catch (Exception ex) {
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    // if (mMarker != null) {
                    // mMarker.remove();
                    // }
                    // mMarker = googleMap.addMarker(new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car)));

                }
            });
            valueAnimator.start();
        }
    }

    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    //Method for finding bearing between two points
    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

//                    LatLng  lattt =marker.getPosition();
//                    float rot= (float) ((lattt.latitude - lattt.latitude) * t + lattt.latitude);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

    private void rotateMarker(final Marker marker, final LatLng destination, final float rotation) {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final float startRotation = marker.getRotation();

            final MarkerAnimation.LatLngInterpolator latLngInterpolator = new MarkerAnimation.LatLngInterpolator.Spherical();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {

                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, destination);
                        float bearing = computeRotation(v, startRotation, rotation);

                        marker.setRotation(bearing);
                        marker.setPosition(newPosition);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            valueAnimator.start();
        }
    }


    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LiveTrackingOnMapActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(!dbHelper.isTableEmpty(DBHelper.DIRECTION_TABLE_NAME)){
                if(dbHelper.checkIfValueExists(trackingId)){
                    dbHelper.updateTableRowEmpty(trackingId,result);
                }else {
                    dbHelper.insertDirections(trackingId,result);

                }

            } else {
                if(dbHelper.isTableEmpty(DBHelper.DIRECTION_TABLE_NAME)){
                    dbHelper.insertDirections(trackingId,result);

                } else {
                    if(!dbHelper.checkIfValueExists(trackingId)){
                        dbHelper.insertDirections(trackingId,result);

                    }
                }


            }
            Log.i("direction Count",dbHelper.getNumOfRows()+"");


            Log.i("Driectuin",dbHelper.getDirections(trackingId)+"");
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);


        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String api = "key=" + "AIzaSyBR3YZ_k07pFYBcEZPLYpDAZvO9NdtOSOU";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&" + api;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            if(jsonData[0]!=null){
                try {
                    jObject = new JSONObject(jsonData[0]);
                    Log.d("ParserTask", jsonData[0].toString());
                    DataParser parser = new DataParser();
                    Log.d("ParserTask", parser.toString());

                    // Starts parsing data
                    routes = parser.parse(jObject);
                    Log.d("ParserTask", "Executing routes");
                    Log.d("ParserTask", routes.toString());

                } catch (Exception e) {
                    Log.d("ParserTask", e.toString());
                    e.printStackTrace();
                }
            }

            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            if(result!=null){
                PolylineOptions lineOptions = null;
                // Traversing through all the routes
                pointspoints.clear();
                if (polyline != null) {
                    polyline.remove();
                }

                for (int i = 0; i < result.size(); i++) {

                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        pointspoints.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(pointspoints);
                    lineOptions.width(10);
                    lineOptions.color(getResources().getColor(R.color.blue));

                    Log.d("onPostExecute", "onPostExecute lineoptions decoded");

                }

                // Drawing polyline in the Google Map for the i-th route
                if (lineOptions != null) {

                    polyline = mMap.addPolyline(lineOptions);
                } else {
                    Log.d("onPostExecute", "without Polylines drawn");
                }
            }
            }

    }

    private class FetchUrl1 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask1 parserTask1 = new ParserTask1();
            // Invokes the thread for parsing the JSON data
            parserTask1.execute(result);


        }
    }

    private class ParserTask1 extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            if (result != null) {
             ArrayList<LatLng> point = new ArrayList<>();
            PolylineOptions lineOptions = null;

            point.clear();
            // Traversing through all the routes

                for (int i = 0; i < result.size(); i++) {

                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point1 = path.get(j);

                        double lat = Double.parseDouble(point1.get("lat"));
                        double lng = Double.parseDouble(point1.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        point.add(position);
                    }
                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(point);
                    lineOptions.width(8);
                    lineOptions.color(getResources().getColor(R.color.dorange));

                    Log.d("onPostExecute", "onPostExecute lineoptions decoded");

                }

                // Drawing polyline in the Google Map for the i-th route
                if (lineOptions != null) {
                    mMap.addPolyline(lineOptions);
                } else {
                    Log.d("onPostExecute", "without Polylines drawn");
                }
            }

        }
    }

    public static LiveTrackingOnMapActivity getInstance() {
        return instance;
    }

    private void animateMarker(GoogleMap myMap, final Marker marker, final List<LatLng> directionPoint,
                               final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = myMap.getProjection();
        final long duration = 600000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            int i = 0;

            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                Location location = new Location(String.valueOf(directionPoint.get(i)));
                Location newlocation = new Location(String.valueOf(directionPoint.get(i + 1)));
                marker.setAnchor(0.5f, 0.5f);
                marker.setRotation(location.bearingTo(newlocation) - 45);
                if (i < directionPoint.size()) {
                    marker.setPosition(directionPoint.get(i));
                }
                i++;

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }


    public void snapLiveRoad(LatLng position, LatLng latLngCurrent) {

        String pathsss = position.latitude + "," + position.longitude + "|" + latLngCurrent.latitude + "," + latLngCurrent.longitude;
        String urlLive = "https://roads.googleapis.com/v1/snapToRoads?path=" + pathsss + "&interpolate=true&key=" + "AIzaSyBR3YZ_k07pFYBcEZPLYpDAZvO9NdtOSOU";
        Log.e("ReqLive", urlLive);
        RequestGenerator.makeGetRequest(LiveTrackingOnMapActivity.this, urlLive, false, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Log.e("Failor", "response failor");
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {
                    Log.e("succes", "response " + string);
                    PolylineOptions lineOptions = new PolylineOptions();
                    ArrayList<LatLng> point = new ArrayList<LatLng>();
                    JSONObject jsonObject = new JSONObject(string);
                    JSONArray jsonArray = jsonObject.getJSONArray("snappedPoints");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        JSONObject jsonObject2 = jsonObject1.getJSONObject("location");

                        double latitude = jsonObject2.getDouble("latitude");
                        double longitude = jsonObject2.getDouble("longitude");
                        LatLng latLng = new LatLng(latitude, longitude);
//                        lineOptions.add(latLng);

                        point.add(latLng);
                    }

                    lineOptions.addAll(point);
                    lineOptions.width(10);
                    lineOptions.color(getResources().getColor(R.color.dorange));
                    mMap.addPolyline(lineOptions);

                }
            }
        });

    }
}
