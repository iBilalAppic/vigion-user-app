package com.vpsappall.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button signInBtn;
    EditText phoneNumber, password;
    TextView forgotPassword;
    AppPreferance preferance;
    CheckBox mCbShowPwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferance = new AppPreferance();
        init();
    }

    private void init() {
        phoneNumber = (EditText) findViewById(R.id.phoneNo);
        password = (EditText) findViewById(R.id.password);
        signInBtn = (Button) findViewById(R.id.signInBtn);
        mCbShowPwd = (CheckBox) findViewById(R.id.cbShowPwd);
        forgotPassword = (TextView) findViewById(R.id.forgotPass);

        signInBtn.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);

        mCbShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // checkbox status is changed from uncheck to checked.
                if (!isChecked) {
                    // show password
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        if (v == signInBtn) {

            if (phoneNumber.getText().toString().equals("")) {
                phoneNumber.setError("Uh Oh! Username Required");
            } /*else if (phoneNumber.getText().toString().length() < 10) {
                phoneNumber.setError("Valid Phone Number Required");
            }*/ else if (password.getText().equals("")) {
                password.setError("Uh Oh! Password Required");
            } else {
                LoginService();
            }

//            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
//            startActivity(intent);
        }
        if (v == forgotPassword) {
            openDialog();
        }

    }


    private void openDialog() {
        final Dialog alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.forgot_layout);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView cancel;
        final EditText regMobile;
        final TextView status, submitBtn;

        regMobile = (EditText) alertDialog.findViewById(R.id.regMobile);
        status = (TextView) alertDialog.findViewById(R.id.status);
        submitBtn = (TextView) alertDialog.findViewById(R.id.subBtn);

        cancel = (ImageView) alertDialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (regMobile.getText().toString().equals("")) {
                    status.setText("Please Enter UserName");
                } else {
                    status.setText("");
                    SendForgotReq(regMobile);
                    alertDialog.cancel();
                }
            }
        });

        alertDialog.show();

    }


    private void SendForgotReq(EditText regMobile) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", regMobile.getText().toString());

            RequestGenerator.makePostRequest(LoginActivity.this, AppConstant.ForgotPassword, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(LoginActivity.this, "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");

                        if (status.equals("success")) {
                            UtilsClass.showToast(LoginActivity.this, message);

                        } else {
                            UtilsClass.showToast(LoginActivity.this, message);
                        }
                    }
                }
            });
        } catch (JSONException e) {
        }
    }

    private void LoginService() {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", phoneNumber.getText().toString());
            jsonObject.put("password", password.getText().toString());
            Log.e("Login Req", jsonObject.toString());

            RequestGenerator.makePostRequest(LoginActivity.this, AppConstant.Login, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(LoginActivity.this, "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");

                        if (status.equals("success")) {
                            UtilsClass.showToast(LoginActivity.this, message);
                            preferance.setPreferences(LoginActivity.this, AppConstant.uniqueId, jsonObject1.getString("id"));
                            preferance.setPreferences(LoginActivity.this, AppConstant.userId, jsonObject1.getString("useruid"));
                            preferance.setPreferences(getApplicationContext(), AppConstant.isLogin, "login");
                            preferance.setPreferences(getApplicationContext(), AppConstant.LoginType, jsonObject1.getString("panel"));
                            preferance.setPreferences(getApplicationContext(), AppConstant.UserLoginType, jsonObject1.getString("user"));
                            preferance.setPreferences(getApplicationContext(), AppConstant.profilePic, jsonObject1.getString("profilepic"));
                            preferance.setPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FLAG, "1");
                            preferance.setPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FOR_HISTORY, "1");
                            preferance.setPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FOR_LIVE_TRACK, "1");
                            preferance.setPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FOR_DETAIL, "1");
                            preferance.setPreferences(getApplicationContext(), AppConstant.username, jsonObject1.getString("name"));
                            preferance.setPreferences(getApplicationContext(), AppConstant.usermail, jsonObject1.getString("email"));

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        } else {
                            UtilsClass.showToast(LoginActivity.this, message);

                        }


                    }
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
