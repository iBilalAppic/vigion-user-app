package com.vpsappall.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfile extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtn;
    ImageButton editImage;
    CircleImageView profilePic;
    TextView nodata;
    EditText userFName, userLName, userPhone, userEmail, userAddrs1, userAddrs2, userCity, userState, userPin;
    LinearLayout allUI;
    Button saveBtn;
    AppPreferance appPreferance;
    String userId, userKaName, userKaMail, userProfilePic, userContact, userAddress, loginType, unId, userLoginType;
    String[] cameraPermision = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
    private static final int PERMISSION_CAMERA_CODE = 12;
    private Uri mImageCaptureUri;
    Bitmap bm;
    byte[] b;
    Bitmap bitmap = null;
    String path = "";
    String putEditImage = "";
    ByteArrayOutputStream baos;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        appPreferance = new AppPreferance();
        userId = appPreferance.getPreferences(EditProfile.this, AppConstant.uniqueId);
        userLoginType = appPreferance.getPreferences(EditProfile.this, AppConstant.UserLoginType);
        unId = appPreferance.getPreferences(EditProfile.this, AppConstant.userId);
        loginType = appPreferance.getPreferences(EditProfile.this, AppConstant.LoginType);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        init();

    }

    private void init() {
        nodata = (TextView) findViewById(R.id.nodata);
        backBtn = (ImageView) findViewById(R.id.editBack);
        editImage = (ImageButton) findViewById(R.id.editImage);
        profilePic = (CircleImageView) findViewById(R.id.edt_ProfileImage);
        userFName = (EditText) findViewById(R.id.et_firstName);
        userLName = (EditText) findViewById(R.id.et_lastName);
        userPhone = (EditText) findViewById(R.id.edt_Phone);
        userEmail = (EditText) findViewById(R.id.edt_email);
        userAddrs1 = (EditText) findViewById(R.id.edt_add1);
        userAddrs2 = (EditText) findViewById(R.id.edt_add2);
        userCity = (EditText) findViewById(R.id.edt_city);
        userState = (EditText) findViewById(R.id.edt_state);
        userPin = (EditText) findViewById(R.id.edt_pin);
        saveBtn = (Button) findViewById(R.id.btn_Save);
        allUI = (LinearLayout) findViewById(R.id.lL_Scroll);

        getProfile();

        editImage.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);

    }

    private void getProfile() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", unId);
            jsonObject.put("type", userLoginType);
            Log.e("get Profile", jsonObject.toString());

            RequestGenerator.makePostRequest(EditProfile.this, AppConstant.GetProfile, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(EditProfile.this, "Network Issue Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");
                        if (status.equals("success")) {
                            Log.e("Profile Res", string);

                            nodata.setVisibility(View.GONE);
                            allUI.setVisibility(View.VISIBLE);

                            userKaName = jsonObject1.getString("firstname") + " " + jsonObject1.getString("lastname");
                            userProfilePic = jsonObject1.getString("profilepic");
                            userAddress = jsonObject1.getString("address1") + " " + jsonObject1.getString("address2") + " " + jsonObject1.getString("city") + " " + jsonObject1.getString("state") + " " + jsonObject1.getString("pincode");
                            userContact = jsonObject1.getString("phone");
                            userKaMail = jsonObject1.getString("email");
                            appPreferance.setPreferences(EditProfile.this, AppConstant.profilePic, userProfilePic);
                            Glide.with(EditProfile.this).load(userProfilePic).into(profilePic);
                            userFName.setText(jsonObject1.getString("firstname"));
                            userLName.setText(jsonObject1.getString("lastname"));
                            if (userLName.getText().toString().equals("")) {
                                userLName.setVisibility(View.GONE);
                            }
                            userPhone.setText(userContact);
                            userEmail.setText(userKaMail);
                            userAddrs1.setText(jsonObject1.getString("address1"));
                            userAddrs2.setText(jsonObject1.getString("address2"));
                            userCity.setText(jsonObject1.getString("city"));
                            userState.setText(jsonObject1.getString("state"));
                            userPin.setText(jsonObject1.getString("pincode"));

                        } else {
                            nodata.setVisibility(View.VISIBLE);
                            allUI.setVisibility(View.GONE);
                            nodata.setText(message);
//                            UtilsClass.showToast(EditProfile.this, message);
                        }
                    }
                }
            });

        } catch (JSONException e) {
        }

    }

    @Override
    public void onClick(View v) {

        if (v == backBtn) {
            Intent intent = new Intent(EditProfile.this, SettingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        if (v == editImage) {
            checkRunTimePermission();
//            selectImage();
        }
        if (v == saveBtn) {
            if (userFName.getText().toString().equals("")) {
                userFName.setError("Firstname Required");
            } else if (userPhone.getText().toString().equals("")) {
                userPhone.setError("Phone Number Required");
            } else if (userEmail.getText().toString().equals("")) {
                userEmail.setError("Email Id Required");
            } else if (userAddrs1.getText().toString().equals("")) {
                userAddrs1.setError("Address 1 Required");
            } else if (userCity.getText().toString().equals("")) {
                userCity.setError("City Required");
            } else if (userState.getText().toString().equals("")) {
                userState.setError("State Required");
            } else if (userPin.getText().toString().equals("")) {
                userPin.setError("Pincode Required");
            } else {
//                UtilsClass.showToast(EditProfile.this, "Working On It");
                SaveDataToServer();
            }
        }
    }

    private void checkRunTimePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!allPermissionsGiven()) {
                requestPermissions(cameraPermision, PERMISSION_CAMERA_CODE);
            } else {
                selectImage();
            }
        } else {
            selectImage();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean allPermissionsGiven() {

        for (String permission : cameraPermision) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CAMERA_CODE) {
            if (grantResults.length > 0) {
                boolean permissionGranted = false;
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(EditProfile.this, getString(R.string.permission_not_granted), Toast.LENGTH_SHORT).show();
                        break;
                    } else {
                        permissionGranted = true;
                    }
                }

                if (permissionGranted) {
                    selectImage();
                }
            }
        }
    }

    private void SaveDataToServer() {
        try {
            JSONObject jsonObject = new JSONObject();

            if (!loginType.toString().equals("transporter")) {
                jsonObject.put("userid", userId);
                jsonObject.put("uid", unId);
                jsonObject.put("type", userLoginType);
                jsonObject.put("firstname", userFName.getText().toString());
                jsonObject.put("lastname", userLName.getText().toString());
                jsonObject.put("address1", userAddrs1.getText().toString());
                jsonObject.put("address2", userAddrs2.getText().toString());
                jsonObject.put("city", userCity.getText().toString());
                jsonObject.put("state", userState.getText().toString());
                jsonObject.put("pincode", userPin.getText().toString());
                jsonObject.put("phone", userPhone.getText().toString());
                jsonObject.put("email", userEmail.getText().toString());
                if (!putEditImage.toString().equals("")) {
                    jsonObject.put("profilepic", "data:image/jpg;base64," + putEditImage);
                } else {
                    jsonObject.put("profilepic", "");
                }
            } else {
                jsonObject.put("userid", userId);
                jsonObject.put("uid", unId);
                jsonObject.put("type", userLoginType);

                jsonObject.put("clientname", userFName.getText().toString());
                jsonObject.put("address1", userAddrs1.getText().toString());
                jsonObject.put("address2", userAddrs2.getText().toString());
                jsonObject.put("city", userCity.getText().toString());
                jsonObject.put("state", userState.getText().toString());
                jsonObject.put("pincode", userPin.getText().toString());
                jsonObject.put("phone", userPhone.getText().toString());
                jsonObject.put("email", userEmail.getText().toString());
                if (!putEditImage.toString().equals("")) {
                    jsonObject.put("profilepic", "data:image/jpg;base64," + putEditImage);
                } else {
                    jsonObject.put("profilepic", "");
                }
            }

            Log.e("Save Req", jsonObject.toString());

            RequestGenerator.makePostRequest(EditProfile.this, AppConstant.SaveProfile, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(EditProfile.this, "Network Issue Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");
                        if (status.equals("success")) {
                            UtilsClass.showToast(EditProfile.this, message);
                            Intent intent = new Intent(EditProfile.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            UtilsClass.showToast(EditProfile.this, message);
                        }
                    }
                }
            });

        } catch (JSONException e) {
        }
    }

    private void selectImage() {

        final String[] items = new String[]{"From Camera", "From SD Card"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(),
                            "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                    mImageCaptureUri = Uri.fromFile(file);

                    try {
                        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                        intent.putExtra("return-data", true);

                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialog.cancel();
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);

                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);

                }
            }
        });

        final AlertDialog dialog = builder.create();

        editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) return;


        if (requestCode == PICK_FROM_FILE) {
            mImageCaptureUri = data.getData();
            path = getRealPathFromURI(mImageCaptureUri); //from Gallery

            if (path == null) {

                path = mImageCaptureUri.getPath(); //from File Manager
                bm = BitmapFactory.decodeFile(path);
                baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                b = baos.toByteArray();

                putEditImage = Base64.encodeToString(b, Base64.DEFAULT);


            } else if (path != null) {

                bitmap = BitmapFactory.decodeFile(path);
                bm = BitmapFactory.decodeFile(path);
                baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                b = baos.toByteArray();
                putEditImage = Base64.encodeToString(b, Base64.DEFAULT);
//                Log.d("path1", putEditImage);
            }
        } else {

            path = mImageCaptureUri.getPath();
            bitmap = BitmapFactory.decodeFile(path);
            bm = BitmapFactory.decodeFile(path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            b = baos.toByteArray();
            putEditImage = Base64.encodeToString(b, Base64.DEFAULT);
//            Log.d("path2", putEditImage);

        }

        profilePic.setImageBitmap(bitmap);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);

        if (cursor == null) return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EditProfile.this, SettingActivity.class);
        startActivity(intent);
        finish();
    }
}
