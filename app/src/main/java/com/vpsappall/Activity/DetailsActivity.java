package com.vpsappall.Activity;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.VolleyError;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView cancel, invoiceView;
    CircleImageView driverImage;
    RelativeLayout imageRL;
    TextView distance, time, driverName, vehicleNumber, packageName, packageWeights, packageGrade, averageSpeedText, etaText;
    LinearLayout chatLL, callLL, invoiceTitleLL, packageNameLL, packageWeightLL, packageGradeLL, etaLL;
    AppPreferance appPreferance;
    String image, trackingId, averaeSpeed, visibleStatus;

    String detailsType, driverProfilePic;
    private String contactNumber;
    private String invoice;
    private String driverId, imageNameFile, loginType;
    private TapTargetSequence tapTargetSequence;
    private String userGuide,userGuidDetail;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        appPreferance = new AppPreferance();
        loginType = appPreferance.getPreferences(getApplicationContext(), AppConstant.LoginType);
        userGuide = appPreferance.getPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FLAG);
        userGuidDetail = appPreferance.getPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FOR_DETAIL);
        trackingId = getIntent().getStringExtra("trackingId");
        detailsType = getIntent().getStringExtra("details");
        Log.e("trackingId", trackingId + " " + detailsType);
        init();


    }

    private void init() {

        cancel = (ImageView) findViewById(R.id.cancelImg);
        invoiceView = (ImageView) findViewById(R.id.invoiceView);

        driverImage = (CircleImageView) findViewById(R.id.driverImage);

        distance = (TextView) findViewById(R.id.distanceText);
        time = (TextView) findViewById(R.id.timeText);
        driverName = (TextView) findViewById(R.id.driverNameText);
        vehicleNumber = (TextView) findViewById(R.id.vehicleNumberText);
        etaText = (TextView) findViewById(R.id.etaText);
        packageName = (TextView) findViewById(R.id.packageNameText);
        packageWeights = (TextView) findViewById(R.id.packageWeightText);
        packageGrade = (TextView) findViewById(R.id.packageGradeText);
        averageSpeedText = (TextView) findViewById(R.id.averageSpeedText);
        invoiceTitleLL = (LinearLayout) findViewById(R.id.invoiceTitleLL);
        packageNameLL = (LinearLayout) findViewById(R.id.packageNameLL);
        packageGradeLL = (LinearLayout) findViewById(R.id.packageGradeLL);
        packageWeightLL = (LinearLayout) findViewById(R.id.packageWeightLL);
        chatLL = (LinearLayout) findViewById(R.id.chatLL);
        callLL = (LinearLayout) findViewById(R.id.callLL);
        etaLL = (LinearLayout) findViewById(R.id.etaLL);
        imageRL = (RelativeLayout) findViewById(R.id.imageInvoice);

        cancel.setOnClickListener(this);
        chatLL.setOnClickListener(this);
        callLL.setOnClickListener(this);
        imageRL.setOnClickListener(this);

        if (detailsType.toString().equals("history")) {
            FetchHistoryDetails();
        } else if (detailsType.toString().equals("live")) {
            Timer timer = new Timer();
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    FetchLiveDetails();
                }
            };
            timer.schedule(task, 0, 10000);
        }

        if (userGuidDetail.equals("1")) {
            final Display display = getWindowManager().getDefaultDisplay();
            final Drawable droid = getResources().getDrawable(R.drawable.ic_dot);
            final Rect droidTarget = new Rect(0, 0, droid.getIntrinsicWidth() * 2, droid.getIntrinsicHeight() * 2);
            droidTarget.offset(display.getWidth() / 2, display.getHeight() / 2);
            tapTargetSequence = new TapTargetSequence(this)
                    .targets(
                            TapTarget.forView(findViewById(R.id.chatLL), "This is Chat Button", "Click here to Chat."),
                            TapTarget.forView(findViewById(R.id.callLL), "This is Call Button", "Click here to Call.")

                                    .dimColor(R.color.black)
                                    .outerCircleColor(R.color.colorPrimary)
                                    .targetCircleColor(R.color.white)
                                    .textColor(R.color.white)
                    )
                    .listener(new TapTargetSequence.Listener() {
                        // This listener will tell us when interesting(tm) events happen in regards
                        // to the sequence
                        @Override
                        public void onSequenceFinish() {
                            // Yay
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        }


                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {
                            // Boo
                        }
                    });
            tapTargetSequence.start();

            appPreferance.setPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FOR_DETAIL, "0");
        }
    }

    private void FetchLiveDetails() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("trackingid", trackingId);
            jsonObject.put("type", loginType);

            Log.e("Live Details Req", jsonObject.toString());

            RequestGenerator.makePostRequest(DetailsActivity.this, AppConstant.LiveTrackingDetails, jsonObject, false, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getApplicationContext(), "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        Log.e("Live Details Response", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");
                        if (status.equals("success")) {
//                            distance.setText(jsonObject1.getString(""));
                            driverName.setText(jsonObject1.getString("drivername"));
                            vehicleNumber.setText(jsonObject1.getString("vehiclenumber"));
                            packageName.setText(jsonObject1.getString("packagename"));
                            packageWeights.setText(jsonObject1.getString("weight") + " " + jsonObject1.getString("weighttype"));
                            time.setText(jsonObject1.getString("timetaken"));
                            packageGrade.setText(jsonObject1.getString("grade"));
                            distance.setText(jsonObject1.getString("distance"));
                            etaText.setText(jsonObject1.getString("etatime"));
                            averageSpeedText.setText(jsonObject1.getString("avgspeed"));
                            contactNumber = jsonObject1.getString("drivernumber");
                            driverProfilePic = jsonObject1.getString("profilepic");
                            driverId = jsonObject1.getString("driverid");
                            invoice = jsonObject1.getString("invoice");
                            visibleStatus = jsonObject1.getString("visible");
                            Glide.with(getApplicationContext()).load(jsonObject1.getString("profilepic")).into(driverImage);
                            Glide.with(getApplicationContext()).load(jsonObject1.getString("invoice")).into(invoiceView);
                            etaLL.setVisibility(View.VISIBLE);
                            if (visibleStatus.equals("0")) {
                                packageNameLL.setVisibility(View.GONE);
                                packageGradeLL.setVisibility(View.GONE);
                                packageWeightLL.setVisibility(View.GONE);
                                invoiceTitleLL.setVisibility(View.GONE);
                                imageRL.setVisibility(View.GONE);

                            } else {
                                packageNameLL.setVisibility(View.VISIBLE);
                                packageGradeLL.setVisibility(View.VISIBLE);
                                packageWeightLL.setVisibility(View.VISIBLE);
                                invoiceTitleLL.setVisibility(View.VISIBLE);
                                imageRL.setVisibility(View.VISIBLE);

                            }

                            calculateDistanceBetween();

                        } else {
                            UtilsClass.showToast(getApplicationContext(), message);
                            contactNumber = "";
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void FetchHistoryDetails() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("trackingid", trackingId);
            jsonObject.put("type", loginType);
            Log.e("History Details Req", jsonObject.toString());

            RequestGenerator.makePostRequest(DetailsActivity.this, AppConstant.HistoryOnMapDetails, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getApplicationContext(), "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        Log.e("Hist Details Response", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");
                        if (status.equals("success")) {
//                            distance.setText(jsonObject1.getString(""));
                            driverName.setText(jsonObject1.getString("drivername"));
                            vehicleNumber.setText(jsonObject1.getString("vehiclenumber"));
                            packageName.setText(jsonObject1.getString("packagename"));
                            packageWeights.setText(jsonObject1.getString("weight") + jsonObject1.getString("weighttype"));
                            time.setText(jsonObject1.getString("timetaken"));
                            packageGrade.setText(jsonObject1.getString("grade"));
                            distance.setText(jsonObject1.getString("distance"));
                            averaeSpeed = jsonObject1.getString("avgspeed");
                            averageSpeedText.setText(jsonObject1.getString("avgspeed"));
                            contactNumber = jsonObject1.getString("drivernumber");
                            invoice = jsonObject1.getString("invoice");
                            driverProfilePic = jsonObject1.getString("profilepic");
                            driverId = jsonObject1.getString("driverid");
                            Glide.with(DetailsActivity.this).load(invoice).into(invoiceView);
                            Glide.with(DetailsActivity.this).load(jsonObject1.getString("profilepic")).into(driverImage);
                            etaLL.setVisibility(View.GONE);

                        } else {
                            UtilsClass.showToast(getApplicationContext(), message);
                            contactNumber = "";
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void calculateDistanceBetween() {


    }

    @Override
    public void onClick(View v) {
        if (v == cancel) {

            onBackPressed();
        }
        if (v == callLL) {
            if (!contactNumber.toString().equals("")) {
                try {
//                        Log.e("Conatct", contactNum);
                    Intent my_callIntent = new Intent(Intent.ACTION_CALL);
                    my_callIntent.setData(Uri.parse("tel:" + contactNumber));

                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(my_callIntent);
                } catch (ActivityNotFoundException e) {
                    e.getMessage();
                }
            } else {
                Toast.makeText(getApplicationContext(), "No Delivery ", Toast.LENGTH_LONG).show();
            }

        }
        if (v == chatLL) {
            if (detailsType.toString().equals("live")) {
                Intent intent = new Intent(DetailsActivity.this, ChatMessageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("type", "live");
                intent.putExtra("trackingid", trackingId);
                intent.putExtra("driverPic", driverProfilePic);
                intent.putExtra("driverId", driverId);
                intent.putExtra("driverName", driverName.getText().toString());
                startActivity(intent);
            } else if (detailsType.toString().equals("history")) {
                Intent intent = new Intent(DetailsActivity.this, ChatMessageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("type", "history");
                intent.putExtra("trackingid", trackingId);
                intent.putExtra("driverPic", driverProfilePic);
                intent.putExtra("driverId", driverId);
                intent.putExtra("driverName", driverName.getText().toString());

                startActivity(intent);
            }
        }

//        if (v == imageRL) {
//            Intent intent = new Intent(DetailsActivity.this, FullScreenImage.class);
//
//            invoiceView.buildDrawingCache();
//            Bitmap image = invoiceView.getDrawingCache();
//
//            Bundle extras = new Bundle();
//            extras.putParcelable("imagebitmap", image);
//            intent.putExtras(extras);
//            startActivity(intent);

//        }

        if (v == imageRL) {

            imageNameFile = invoice;
            final Dialog alertDialog = new Dialog(DetailsActivity.this);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.showimage_ui);

            ImageView imageView = (ImageView) alertDialog.findViewById(R.id.IMAGEID);


       /*     Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (this != null && !isFinishing()) {
                        final ProgressDialog dialog = ProgressDialog.show(DetailsActivity.this,
                                "Please Wait... ", "Loading... ", false, true);
                        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    }
                }
            }, 3000);*/
            Picasso.with(DetailsActivity.this).load(imageNameFile).into(imageView);


            alertDialog.show();


        }

    }

}
