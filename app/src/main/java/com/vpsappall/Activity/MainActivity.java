package com.vpsappall.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.vpsappall.Fragment.HistoryFragment;
import com.vpsappall.Fragment.HomeLiveFragment;
import com.vpsappall.Fragment.TransporterLiveFragment;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import cn.tovi.CustomMenu;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    private static MainActivity instance;
    TextView tvTitle, tvTitle1, viewe1, viewe2;
    private CustomMenu slidingMenu;
    private View leftMenuView;
    ImageView menuImage, logoutImage;
    CircleImageView profilePic;
    TextView userName;
    private TextView notiText, settingText, aboutText, privacyText;
    AppPreferance appPreferance;
    String userId, profilePicSaved, userNamee, loginType, unId, loginSubType, userGuide;
    private static long back_pressed;
    TapTargetSequence tapTargetSequence;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = MainActivity.this;
        instance = MainActivity.this;
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        appPreferance = new AppPreferance();


        loginType = appPreferance.getPreferences(getApplicationContext(), AppConstant.LoginType);
        loginSubType = appPreferance.getPreferences(getApplicationContext(), AppConstant.UserLoginType);
        userId = appPreferance.getPreferences(getApplicationContext(), AppConstant.uniqueId);
        unId = appPreferance.getPreferences(getApplicationContext(), AppConstant.userId);
        userGuide = appPreferance.getPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FLAG);
//        profilePicSaved = appPreferance.getPreferences(getApplicationContext(), AppConstant.profilePic);
//        userNamee = appPreferance.getPreferences(getApplicationContext(), AppConstant.username);

        slidingMenu = new CustomMenu(this);
        slidingMenu.setContentView(R.layout.activity_main);
        leftMenuView = getLayoutInflater().inflate(R.layout.layout_left_menu, slidingMenu, false);
        slidingMenu.setLeftMenu(leftMenuView);
        setContentView(slidingMenu);

        profilePic = (CircleImageView) findViewById(R.id.navhead_image);
        userName = (TextView) findViewById(R.id.navhead_name);

        notiText = (TextView) findViewById(R.id.notiText);
        settingText = (TextView) findViewById(R.id.settingText);
        aboutText = (TextView) findViewById(R.id.aboutText);
        privacyText = (TextView) findViewById(R.id.privacyText);
        logoutImage = (ImageView) findViewById(R.id.logoutImage);

        tvTitle = (TextView) findViewById(R.id.titleLive);
        tvTitle1 = (TextView) findViewById(R.id.titleHistory);
        viewe1 = (TextView) findViewById(R.id.view1);
        viewe2 = (TextView) findViewById(R.id.view2);
        menuImage = (ImageView) findViewById(R.id.button_menu);

        getProfile();


        if (savedInstanceState == null) {
            if (loginType.toString().equals("customer")) {
                getSupportFragmentManager().beginTransaction().add(R.id.Tab_Container, new HomeLiveFragment()).commit();
            } else if (loginType.toString().equals("transporter") || loginType.toString().equals("client")) {
                getSupportFragmentManager().beginTransaction().add(R.id.Tab_Container, new TransporterLiveFragment()).commit();
            }
        }


        menuImage.setOnClickListener(this);
        tvTitle.setOnClickListener(this);
        tvTitle1.setOnClickListener(this);
        notiText.setOnClickListener(this);
        settingText.setOnClickListener(this);
        aboutText.setOnClickListener(this);
        privacyText.setOnClickListener(this);
        logoutImage.setOnClickListener(this);

        generateToken();

        if (userGuide.equals("1")) {
            final Display display = getWindowManager().getDefaultDisplay();
            final Drawable droid = getResources().getDrawable(R.drawable.ic_dot);
            final Rect droidTarget = new Rect(0, 0, droid.getIntrinsicWidth() * 2, droid.getIntrinsicHeight() * 2);
            droidTarget.offset(display.getWidth() / 2, display.getHeight() / 2);
            tapTargetSequence = new TapTargetSequence(this)
                    .targets(
                            TapTarget.forView(findViewById(R.id.button_menu), "This is Menu Button", "Click here to open Menu."),
                            TapTarget.forView(findViewById(R.id.titleHistory), "This is History", "Click here to see History Of Tracking List .")

                                    .dimColor(R.color.black)
                                    .outerCircleColor(R.color.colorPrimary)
                                    .targetCircleColor(R.color.white)
                                    .textColor(R.color.white),
                            TapTarget.forBounds(droidTarget, "Click on Blue Dot", "Click To See Live Tracking Detail Page ")
                                    .cancelable(false)
                    )
                    .listener(new TapTargetSequence.Listener() {
                        // This listener will tell us when interesting(tm) events happen in regards
                        // to the sequence
                        @Override
                        public void onSequenceFinish() {
                            // Yay
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        }


                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {
                            // Boo
                        }
                    });
            tapTargetSequence.continueOnCancel(true);
            tapTargetSequence.start();
            appPreferance.setPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FLAG, "0");

        }
//        getAllDataOfUser()


    }


    public void generateToken() {
//        new GenerateDeviceToken(MainActivity.this).execute();
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = token;
                        Log.d("tokenGenerated", msg);
                        sendTokenToServer(msg);
//                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });


    }

    public void sendTokenToServer(String token) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", userId);
            jsonObject.put("type", loginSubType);
            jsonObject.put("devicetype", "ANDROID");
            jsonObject.put("devicetoken", token);

            Log.e("Token Update Req", jsonObject.toString());

            RequestGenerator.makePostRequest(MainActivity.this, AppConstant.TokenUpdate, jsonObject,
                    false, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(MainActivity.this, "Oops ! Token Updation Failed");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");

                        if (status.equals("success")) {
                            UtilsClass.showLog(message);
                        } else {
                            UtilsClass.showLog(message);
                        }
                    }
                }
            });

        } catch (JSONException e) {
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public void onClick(View v) {

        if (v == tvTitle) {

            tvTitle1.setTextColor(Color.parseColor("#BCBCBC"));
            tvTitle.setTextColor(Color.parseColor("#000000"));
            viewe1.setVisibility(View.VISIBLE);
            viewe2.setVisibility(View.GONE);
            if (loginType.toString().equals("customer")) {
                getSupportFragmentManager().beginTransaction().add(R.id.Tab_Container, new HomeLiveFragment()).commit();
            } else if (loginType.toString().equals("transporter") || loginType.toString().equals("client")) {
                getSupportFragmentManager().beginTransaction().add(R.id.Tab_Container, new TransporterLiveFragment()).commit();
            }
        }

        if (v == tvTitle1) {

            tvTitle1.setTextColor(Color.parseColor("#000000"));
            tvTitle.setTextColor(Color.parseColor("#BCBCBC"));
            viewe1.setVisibility(View.GONE);
            viewe2.setVisibility(View.VISIBLE);
            UtilsClass.goToFragment(MainActivity.this, new HistoryFragment(), R.id.Tab_Container, false);
        }

        if (v == menuImage) {
            toggleSlidingMenu();

        }
        if (v == notiText) {
            slidingMenu.closeMenu();

            Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
            startActivity(intent);
            finish();
        }

        if (v == settingText) {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(intent);
            finish();
        }
        if (v == aboutText) {
//            UtilsClass.showToast(mContext, "Work To Be Done");
        }
        if (v == privacyText) {
//            UtilsClass.showToast(mContext, "Work To Be Done");
        }
        if (v == logoutImage) {
            slidingMenu.closeMenu();
            UtilsClass.showDialog(MainActivity.this, getString(R.string.confirm), getString(R.string.dial_titlelogout), getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    UtilsClass.showToast(MainActivity.this, "Successfully Logout");
                    RemoveTokenFormServer();
                    appPreferance.clearSharedPreference(MainActivity.this);
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();


                }
            }, null, getString(R.string.cancel), null, null, false);
        }
    }

    private void RemoveTokenFormServer() {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", userId);
            jsonObject.put("type", loginSubType);
            UtilsClass.showLog(jsonObject.toString());

            RequestGenerator.makePostRequest(MainActivity.this, AppConstant.DeleteToken, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(MainActivity.this, "Token Deletion Failed");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");

                        if (status.equals("success")) {
                            UtilsClass.showToast(MainActivity.this, message);
                        } else {
                            UtilsClass.showToast(MainActivity.this, message);
                        }
                    }
                }
            });

        } catch (JSONException e) {
        }


    }

    private void toggleSlidingMenu() {
        if (slidingMenu.getState() == CustomMenu.State.CLOSE_MENU) {
            slidingMenu.openLeftMenuIfPossible();
        } else if (slidingMenu.getState() == CustomMenu.State.LEFT_MENU_OPENS) {
            slidingMenu.closeMenu();
        }
    }

    private void getProfile() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", unId);
            jsonObject.put("type", loginSubType);
            Log.e("get Profile", jsonObject.toString());

            RequestGenerator.makePostRequest(MainActivity.this, AppConstant.GetProfile, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(MainActivity.this, "Network Issue Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");
                        if (status.equals("success")) {
                            Log.e("Profile Res", string);
                            appPreferance.setPreferences(getApplicationContext(), AppConstant.uniqueId, jsonObject1.getString("userid"));
                            appPreferance.setPreferences(getApplicationContext(), AppConstant.profilePic, jsonObject1.getString("profilepic"));
                            appPreferance.setPreferences(getApplicationContext(), AppConstant.username, jsonObject1.getString("firstname") + " " + jsonObject1.getString("lastname"));


                            Glide.with(MainActivity.this).load(jsonObject1.getString("profilepic"))
                                    .into(profilePic);

                            userName.setText(jsonObject1.getString("firstname") + " " + jsonObject1.getString("lastname"));


                        } else {
                            UtilsClass.showToast(MainActivity.this, message);
                        }
                    }
                }
            });

        } catch (JSONException e) {
        }

    }

    @Override
    public void onBackPressed() {

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.Tab_Container);
        if (slidingMenu.getState() == CustomMenu.State.LEFT_MENU_OPENS) {
            slidingMenu.closeMenu();
        } else if (f instanceof HistoryFragment) {

            tvTitle1.setTextColor(Color.parseColor("#BCBCBC"));
            tvTitle.setTextColor(Color.parseColor("#000000"));
            viewe1.setVisibility(View.VISIBLE);
            viewe2.setVisibility(View.GONE);
            if (loginType.toString().equals("customer")) {
                getSupportFragmentManager().beginTransaction().add(R.id.Tab_Container, new HomeLiveFragment()).commit();
            } else if (loginType.toString().equals("transporter") || loginType.toString().equals("client")) {
                getSupportFragmentManager().beginTransaction().add(R.id.Tab_Container, new TransporterLiveFragment()).commit();
            }
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()) {

                finish();
                super.onBackPressed();
            } else {
                Snackbar snackbar = Snackbar.make(menuImage, "Press once again to close app.", Snackbar.LENGTH_SHORT);
                View snackbarView = snackbar.getView();
                TextView textView = (TextView) snackbarView.findViewById(R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbarView.setBackgroundColor(Color.DKGRAY);
                snackbar.show();
                back_pressed = System.currentTimeMillis();

            }
        }
    }

    public static MainActivity getInstance() {
        return instance;
    }

}