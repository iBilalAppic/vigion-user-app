package com.vpsappall.Activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;

import com.android.volley.VolleyError;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vpsappall.Model.HistoryOnMapModel;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HistoryOnMap extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private static final String TAG = HistoryOnMap.class.getSimpleName();
    ImageButton imageButton;
    private GoogleMap mMap;
    String trackingId, newTrackingId;
    ArrayList<HistoryOnMapModel> arrayList = new ArrayList<>();
    FloatingActionButton fab;
    private ArrayList<LatLng> points = new ArrayList<LatLng>();
    String paths, loginType, userGuide,userGuideHistory;
    AppPreferance appPreferance;
    private TapTargetSequence tapTargetSequence;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_on_map);
        appPreferance = new AppPreferance();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        trackingId = getIntent().getStringExtra("trackingId");
        loginType = appPreferance.getPreferences(getApplicationContext(), AppConstant.LoginType);
        userGuide = appPreferance.getPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FLAG);
        userGuideHistory = appPreferance.getPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FOR_HISTORY);
        ShowHistoryRoute();

        fab = (FloatingActionButton) findViewById(R.id.fab_btn);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HistoryOnMap.this, DetailsActivity.class);
                intent.putExtra("trackingId", newTrackingId);
                intent.putExtra("details", "history");
                startActivity(intent);
            }
        });

        imageButton = (ImageButton) findViewById(R.id.backImg1);
        imageButton.setOnClickListener(this);

        if (userGuideHistory.equals("1")) {
            final Display display = getWindowManager().getDefaultDisplay();
            final Drawable droid = getResources().getDrawable(R.drawable.ic_dot);
            final Rect droidTarget = new Rect(0, 0, droid.getIntrinsicWidth() * 2, droid.getIntrinsicHeight() * 2);
            droidTarget.offset(display.getWidth() / 2, display.getHeight() / 2);
            tapTargetSequence = new TapTargetSequence(this)
                    .targets(
                            TapTarget.forView(findViewById(R.id.backImg1), "This is Back Button", "Click here to go back."),
                            TapTarget.forView(findViewById(R.id.fab_btn), "More Detail Button", "Click here to see More Details Of Tracking .")

                                    .dimColor(R.color.black)
                                    .outerCircleColor(R.color.colorPrimary)
                                    .targetCircleColor(R.color.white)
                                    .textColor(R.color.white)
                    )
                    .listener(new TapTargetSequence.Listener() {
                        // This listener will tell us when interesting(tm) events happen in regards
                        // to the sequence
                        @Override
                        public void onSequenceFinish() {
                            // Yay
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        }


                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {
                            // Boo

                        }
                    });

            tapTargetSequence.continueOnCancel(true);
            tapTargetSequence.start();
            appPreferance.setPreferences(getApplicationContext(), AppConstant.USER_GUIDE_FOR_HISTORY, "0");

        }
    }


    private void ShowHistoryRoute() {


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("trackingid", trackingId);
            jsonObject.put("type", loginType);

            Log.e("RequestHist", jsonObject.toString());

            RequestGenerator.makePostRequest(HistoryOnMap.this, AppConstant.HistoryOnMap, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getApplicationContext(), "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        Log.e("ResponseHist", string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");
                        if (status.equals("success")) {

                            JSONArray jsonArray = jsonObject1.getJSONArray("trackhistorymap");
                            arrayList.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                HistoryOnMapModel historyOnMapModel = new HistoryOnMapModel();
                                historyOnMapModel.setTrackingId(jsonObject2.getString("trackingid"));
                                newTrackingId = jsonObject2.getString("trackingid");
                                JSONArray jsonArray1 = jsonObject2.getJSONArray("navigation");
                                ArrayList<HistoryOnMapModel.NavigationData> navigationDatas = new ArrayList<HistoryOnMapModel.NavigationData>();
                                for (int j = 0; j < jsonArray1.length(); j++) {
                                    JSONObject jsonObject3 = jsonArray1.getJSONObject(j);

                                    HistoryOnMapModel.NavigationData navigationData = historyOnMapModel.new NavigationData();

                                    navigationData.setLongitude(jsonObject3.getString("longitude"));
                                    navigationData.setLatitude(jsonObject3.getString("latitude"));
                                    navigationDatas.add(navigationData);

                                }
                                historyOnMapModel.setData(navigationDatas);

                                arrayList.add(historyOnMapModel);
                            }

                            onMapReady(mMap);

                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int hour = cal.get(Calendar.HOUR_OF_DAY); //Get the hour from the calendar
//        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        try {
            boolean success;
            if (hour <= 17 && hour >= 6) {
                success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json_light));

            } else {

                success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
            }
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        LatLng latLngStart = null, latLngEnd = null;
        ArrayList<HistoryOnMapModel.NavigationData> arrayList12;
        ArrayList<List<HistoryOnMapModel.NavigationData>> partitions = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {
            StringBuilder s = new StringBuilder();
            arrayList12 = arrayList.get(i).getData();
            LatLng latLng = null;
            for (int j = 0; j < arrayList12.size(); j++) {
                if (!arrayList12.isEmpty()) {
                    Double lat = Double.valueOf(arrayList12.get(j).getLatitude());
                    Double lang = Double.valueOf(arrayList12.get(j).getLongitude());
                    latLng = new LatLng(lat, lang);
                    points.add(latLng);

                    latLngStart = new LatLng(Double.valueOf(arrayList12.get(0).getLatitude()), Double.valueOf(arrayList12.get(0).getLongitude()));
                    latLngEnd = new LatLng(Double.valueOf(arrayList12.get(arrayList12.size() - 1).getLatitude()), Double.valueOf(arrayList12.get(arrayList12.size() - 1).getLongitude()));

                }

                MarkerOptions markerOptions = new MarkerOptions().position(latLngStart).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).title("Start");
                MarkerOptions markerOptions1 = new MarkerOptions().position(latLngEnd).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title("End");
                mMap.addMarker(markerOptions);
                mMap.addMarker(markerOptions1);

                fab.setVisibility(View.VISIBLE);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(13));
            }

            if (points.size() > 99) {

                for (int k = 0; k < 100; k++) {
                    s.append(arrayList12.get(k).getLatitude());
                    s.append(",");
                    s.append(arrayList12.get(k).getLongitude());
                    s.append("|");
                }
                if (s.length() > 0) {
                    s.setLength(s.length() - 1);
                }
                paths = s.toString();
                callSnapToRoad();

            } else {
                if (points.size() <= 0) {

                } else {
                    for (int k = 0; k < points.size(); k++) {
                        s.append(arrayList12.get(k).getLatitude());
                        s.append(",");
                        s.append(arrayList12.get(k).getLongitude());
                        s.append("|");
                    }
                    if (s.length() > 0) {
                        s.setLength(s.length() - 1);
                    }
                    paths = s.toString();
                    callSnapToRoad();
                }
            }


        }


    }

    public void forLoopMethod() {
        StringBuilder s = new StringBuilder();


        if (points.size() > 99) {

            for (int k = 0; k < 100; k++) {

                s.append(points.get(k).latitude);
                s.append(",");
                s.append(points.get(k).longitude);
                s.append("|");
            }
            if (s.length() > 0) {
                s.setLength(s.length() - 1);
            }
            paths = s.toString();
            callSnapToRoad();

        } else {
            if (points.size() <= 0) {

            } else {
                for (int k = 0; k < points.size(); k++) {

                    s.append(points.get(k).latitude);
                    s.append(",");
                    s.append(points.get(k).longitude);
                    s.append("|");


                }
                if (s.length() > 0) {
                    s.setLength(s.length() - 1);
                }
                paths = s.toString();
                callSnapToRoad();
            }
        }


    }

    private void callSnapToRoad() {
        String snapToRoadUrl = "https://roads.googleapis.com/v1/snapToRoads?path=" + paths + "&interpolate=true&key=" + "AIzaSyBR3YZ_k07pFYBcEZPLYpDAZvO9NdtOSOU";
        RequestGenerator.makeGetRequest(HistoryOnMap.this, snapToRoadUrl, false, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Log.e("Failor", "response failor");
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                if (!string.equals("")) {
                    PolylineOptions lineOptions = new PolylineOptions();
                    ArrayList<LatLng> point = new ArrayList<LatLng>();
                    JSONObject jsonObject = new JSONObject(string);
                    JSONArray jsonArray = jsonObject.getJSONArray("snappedPoints");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        JSONObject jsonObject2 = jsonObject1.getJSONObject("location");

                        double latitude = jsonObject2.getDouble("latitude");
                        double longitude = jsonObject2.getDouble("longitude");
                        LatLng latLng = new LatLng(latitude, longitude);

                        point.add(latLng);
                    }

                    lineOptions.addAll(point);
                    lineOptions.width(8);
                    lineOptions.color(getResources().getColor(R.color.dblue));
                    mMap.addPolyline(lineOptions);
                    if (points.size() > 99) {
                        points.subList(0, 99).clear();
                    } else {
                        if (points.size() <= 0) {

                        } else {
                            points.subList(0, points.size()).clear();
                        }
                    }
                    forLoopMethod();

                } else {
                    Log.e("succes", "response Empty");
                }

            }
        });

    }


    @Override
    public void onClick(View v) {
        if (v == imageButton) {
            onBackPressed();
        }

    }


}
