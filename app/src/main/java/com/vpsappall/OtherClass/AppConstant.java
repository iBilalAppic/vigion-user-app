package com.vpsappall.OtherClass;

/**
 * Created by Bilal Khan on 16/6/17.
 */

public class AppConstant {


    //    public static String MainUrl = "http://35.154.228.79/beacon/mobileservicecustomer.php?servicename=";
//    public static String MainUrl = "http://35.154.112.164/service/app1.0/mobileservicecustomer.php?servicename=";
    public static String MainUrl = "http://13.127.114.214/beacon/service/app1.1/mobileservicecustomer.php?servicename=";

    public static String GetSlider = MainUrl + "slider";
    public static String Login = MainUrl + "login";
    //    public static String LocationUpdate = MainUrl + "updatelocation";
    public static String TrackingList = MainUrl + "tracking";

    public static String TranspoterTrackList = MainUrl + "livetrackinglist";

    public static String LiveTracking = MainUrl + "livetracking";
    public static String LiveTrackingDetails = MainUrl + "trackingdetail";

    public static String ListOfHistory = MainUrl + "trackhistory";

    public static String HistoryOnMap = MainUrl + "trackhistorymap";
    public static String HistoryOnMapDetails = MainUrl + "trackingdetailpast";

    public static String ListOfNotifications = MainUrl + "notification";

    public static String EndTracking = MainUrl + "endtracking";

    public static String TokenUpdate = MainUrl + "updatetoken";
    public static String DeleteToken = MainUrl + "deletetoken";
    public static String ForgotPassword = MainUrl + "forgetpassword";

    public static String SendMessage = MainUrl + "sendmessage";
    public static String HistoryChat = MainUrl + "chatmessage";
    public static String GetProfile = MainUrl + "getprofile";

    public static String ChangePassword = MainUrl + "changepassword";
    public static String SaveProfile = MainUrl + "saveprofile";
    public static String ChatSeenStatus = MainUrl + "chatseen";

    public static String deviceToken = "devicetoken";
    public static String userId = "userId";
    public static String uniqueId = "id";
    public static final String isLogin = "login";
    public static final String LoginType = "logintype";
    public static final String UserLoginType = "user";
    public static final String profilePic = "profilepic";
    public static final String username = "name";
    public static final String usermail = "email";
    public static String getNoti = "getNoti";
    public static String defaultRingtone = "ring";
    public static String ringtoneName = "nameR";


    public static String notification_key_message = "message";
    public static String notification_key_type = "type";
    public static String notification_key_name = "title";
    public static String notification_key_number = "number";
    public static String notification_key_server_id = "server_contactId";
    public static String notification_key_image_url = "image";

    public static String USER_GUIDE_FLAG = "userguide";
    public static String USER_GUIDE_FOR_HISTORY = "userguide_for_hist";
    public static String USER_GUIDE_FOR_LIVE_TRACK = "userguide_for_live_track";
    public static String USER_GUIDE_FOR_DETAIL = "userguide_for_live_track";
}
