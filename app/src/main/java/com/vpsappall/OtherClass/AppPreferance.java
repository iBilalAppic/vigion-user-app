package com.vpsappall.OtherClass;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Bilal Khan on 16/6/17.
 */

public class AppPreferance {


    public void setPreferences(Context context, String key, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("Bilal Khan", Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();

    }


    public String getPreferences(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("Bilal Khan", Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }


    public void clearSharedPreference(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences("Bilal Khan", Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
    }
}
