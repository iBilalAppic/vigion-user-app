package com.vpsappall.OtherClass;

import android.content.Context;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Bilal Khan on 2/8/17.
 */
public class DateTimeUtils {

    public static final String INVALID_DATE = "Invalid date";

    public static String convertDateTime(String createdTime) {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Date past = null;
        try {
            past = format.parse(createdTime);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Date now = new Date();


        long diff = now.getTime() - past.getTime();

        String time = null;
        long days = diff / (24 * 60 * 60 * 1000);

        if (days == 0) {
            time = "Today" + formateDate(createdTime);
        } else if (days == 1) {
            time = "Yesterday" + formateDate(createdTime);
        /*else if (days <= 7) {
            time = days + " days ago"+formateDate(createdTime);
        } else if (days > 30) {
            long _days = days / 30;
            time = String.valueOf(_days) + " month ago";
        } else {
            long _days = days / 7;
            time = String.valueOf(_days) + " weeks ago";*/
        } else {
            time = createdTime;
        }
        return time;
    }

    public static String formatToYesterdayOrToday(Context context, String date) throws ParseException {
        Date dateTime = new SimpleDateFormat("MM/dd/yyyy hh:mm").parse(date);
        Date now = new Date();
        String str = DateUtils.getRelativeTimeSpanString(
                dateTime.getTime(),
                now.getTime(),
                0,
                DateUtils.FORMAT_ABBREV_ALL).toString();
        return str;
    }

    public static String formateDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Date newDate = null;
        try {
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("hh:mm");
        String formatedDate = format.format(newDate);
        return formatedDate;
    }

    public static int getDayFromString(String date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDateFromString(date, "dd/MM/yyyy hh:mm:ss"));
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static String getStringFromDate(Date date, String dateFormat) {
        try {
            return new SimpleDateFormat(dateFormat).format(date);
        } catch (Exception e) {
            return INVALID_DATE;
        }
    }

    public static Date getDateFromString(String date, String dateFormat) {

        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

}
