package com.vpsappall.OtherClass;

/**
 * Created by Bilal Khan on 1/8/17.
 */

public class ExtraWork {


}



/*package com.beacon.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.beacon.Adapter.TransporterAdapter;
import com.beacon.Model.LiveTruckModel;
import com.beacon.OtherClass.AppConstant;
import com.beacon.OtherClass.AppPreferance;
import com.beacon.OtherClass.UtilsClass;
import com.beacon.R;
import com.beacon.Services.RequestGenerator;
import com.beacon.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/*
public class TransporterLiveFragment extends Fragment {

    Context context;
    private static com.beacon.Fragment.TransporterLiveFragment instance;
    RecyclerView transporterRecyc;
    TextView noDataHistory;
    LinearLayoutManager layoutManager;

    AppPreferance appPreferance;
    TransporterAdapter adapter;
    ArrayList<LiveTruckModel> arrayList = new ArrayList<>();
    String userId;

    public TransporterLiveFragment() {
        // Required empty public constructor
        instance = com.beacon.Fragment.TransporterLiveFragment.this;
        context = getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transporter_live, container, false);

        appPreferance = new AppPreferance();
        userId = appPreferance.getPreferences(getContext(), AppConstant.uniqueId);

        noDataHistory = (TextView) view.findViewById(R.id.noDataTrack);
        transporterRecyc = (RecyclerView) view.findViewById(R.id.transporterRecyc);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        transporterRecyc.setLayoutManager(layoutManager);
        initUI();

        return view;

    }

    public void initUI() {
        FetchLiveVehicleList();
    }

    private void FetchLiveVehicleList() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transporterid", userId);


            RequestGenerator.makePostRequest(getActivity(), AppConstant.TranspoterTrackList, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getContext(), "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        Log.e("Trans Truck Res", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String message = jsonObject1.getString("message");

                        if (status.equals("success")) {
                            JSONArray jsonArray = jsonObject1.getJSONArray("tracking");
                            arrayList.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                LiveTruckModel liveTruckModel = new LiveTruckModel();
                                liveTruckModel.setPackageid(jsonObject2.getString("packageid"));
                                liveTruckModel.setDriverid(jsonObject2.getString("driverid"));
                                liveTruckModel.setDrivername(jsonObject2.getString("drivername"));
                                liveTruckModel.setDriverpic(jsonObject2.getString("driverpic"));
                                liveTruckModel.setVehicleid(jsonObject2.getString("vehicleid"));
                                liveTruckModel.setVehiclenumber(jsonObject2.getString("vehiclenumber"));
                                liveTruckModel.setDestination(jsonObject2.getString("destination"));
                                liveTruckModel.setLatitude(jsonObject2.getString("latitude"));
                                liveTruckModel.setLongitude(jsonObject2.getString("longitude"));
                                arrayList.add(liveTruckModel);
                            }

                            noDataHistory.setVisibility(View.GONE);

                            adapter = new TransporterAdapter(getActivity(), arrayList);
                            transporterRecyc.setAdapter(adapter);
                            UtilsClass.showToast(getContext(), message);
                            noDataHistory.setText(message);

                        } else {
                            noDataHistory.setVisibility(View.VISIBLE);
                            transporterRecyc.setVisibility(View.GONE);
                            noDataHistory.setText(message);
                        }


                    }
                }
            });

        } catch (JSONException e) {
        }

//
//
//


    }

    public static com.beacon.Fragment.TransporterLiveFragment getInstance() {
        return instance;
    }


}
*/