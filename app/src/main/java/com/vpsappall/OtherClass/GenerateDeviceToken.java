package com.vpsappall.OtherClass;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.InstanceIdResult;
import com.vpsappall.Activity.MainActivity;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by Bilal Khan on 16/6/17.
 */

public class GenerateDeviceToken extends AsyncTask<String, Void, String> {

    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";
    private static final String TAG = "RegIntentService";
    public Context context;
    private String device_token;
    private AppPreferance appPreferences;

    public GenerateDeviceToken(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        if (registerGCM()) {
            return "true";
        } else {
            return "false";
        }
    }


    protected void onPreExecute() {

    }

    protected void onPostExecute(String result) {
        if (result.equals("true")) {
            appPreferences = new AppPreferance();
            appPreferences.setPreferences(context, AppConstant.deviceToken, device_token);

        } else {
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
        }
    }

    private Boolean registerGCM() {

//         device_token = String.valueOf(FirebaseInstanceId.getInstance().getInstanceId());
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = token;
                        Log.d(TAG, msg);
//                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        // [END get_token]
        Log.e(TAG, "GCM Registration Token: " + device_token);
        sendTokenToServer(device_token);
        if (device_token == null || device_token.isEmpty()) {
            return false;
        } else {
            return true;
        }


    }

    private void sendTokenToServer(String device_token) {
//        final MainActivity homeActivity = MainActivity.getInstance();
//        homeActivity.sendTokenToServer(device_token);

        ((MainActivity) context).sendTokenToServer(device_token);

    }
}
