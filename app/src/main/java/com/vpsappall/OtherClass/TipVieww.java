package com.vpsappall.OtherClass;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Bilal Khan on 27/6/17.
 */

public class TipVieww extends View {

    private Paint a = new Paint();
    private Path b = new Path();

    public TipVieww(Context var1, AttributeSet var2) {
        super(var1, var2);

        this.a.setColor(Color.parseColor("#ffffff"));
        this.a.setAntiAlias(true);
        this.a.setStrokeWidth(0.0F);
        this.a.setStyle(Paint.Style.FILL);
    }

    public void setColor(int var1) {
        this.a.setColor(var1);
    }

    protected void onDraw(Canvas var1) {
        int var2 = this.getMeasuredHeight();
        int var3 = this.getMeasuredWidth();
        this.b.rewind();
        this.b.moveTo((float) (var3 / 2 - var2), 0.0F);
        this.b.lineTo((float) (var3 / 2 + var2), 0.0F);
        this.b.lineTo((float) (var3 / 2), (float) var2);
        this.b.lineTo((float) (var3 / 2 - var2), 0.0F);
        var1.drawPath(this.b, this.a);
    }
}