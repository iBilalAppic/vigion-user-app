package com.vpsappall.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vpsappall.Activity.SplashActivity;
import com.vpsappall.Model.SliderModel;
import com.vpsappall.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 31/5/17.
 */

public class SplashAdapter extends RecyclerView.Adapter<SplashAdapter.ViewHolder> {

    Context context;
    LayoutInflater inflater;
    private ArrayList<SliderModel> ImagesArray = new ArrayList<SliderModel>();

    public SplashAdapter(SplashActivity splashActivity, ArrayList<SliderModel> imagesArray) {
        this.context = splashActivity;
        this.ImagesArray = imagesArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_shop_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        SliderModel sliderModel = ImagesArray.get(position);

        Glide.with(context).load(sliderModel.getSliderImage()).into(holder.image);
        holder.tettt.setText(sliderModel.getSliderText());


    }

    @Override
    public int getItemCount() {
        return ImagesArray.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView tettt;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            tettt = (TextView) itemView.findViewById(R.id.title2);
        }
    }
}
