package com.vpsappall.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.vpsappall.Activity.HistoryOnMap;
import com.vpsappall.Model.HistoryModel;
import com.vpsappall.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Bilal Khan on 9/6/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    Context context;
    ArrayList<HistoryModel> arrayList = new ArrayList<>();
    private String historyId;

    public HistoryAdapter(FragmentActivity historyFragment, ArrayList<HistoryModel> arrayList) {
        this.context = historyFragment;
        this.arrayList = arrayList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_history_card, parent, false);
        return new HistoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        HistoryModel historyModel = arrayList.get(position);


        Glide.with(context).load(historyModel.getDriverPic()).into(holder.profile);
        holder.packageName.setText(historyModel.getPackageName());
        holder.driverName.setText(historyModel.getDriverName());
        holder.vehicleNumber.setText(historyModel.getVehicleNumber());

        if (holder.packageName.getText().toString().equals("")) {
            holder.driverName.setTextSize(18);
            holder.driverName.setGravity(Gravity.BOTTOM);
            holder.driverName.setTypeface(holder.driverName.getTypeface(), Typeface.BOLD);
            holder.packageName.setVisibility(View.GONE);
        } else {
            holder.packageName.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView profile;
        TextView packageName, driverName, vehicleNumber;


        public ViewHolder(View itemView) {
            super(itemView);

            profile = (CircleImageView) itemView.findViewById(R.id.historyPic);
            packageName = (TextView) itemView.findViewById(R.id.historyPackageName);
            driverName = (TextView) itemView.findViewById(R.id.historyDriverName);
            vehicleNumber = (TextView) itemView.findViewById(R.id.historyVehicleNumber);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == itemView) {

                historyId = arrayList.get(getAdapterPosition()).getTrackingId();
                Intent intent = new Intent(context, HistoryOnMap.class);
                intent.putExtra("trackingId", historyId);

                context.startActivity(intent);
            }

        }
    }
}
/*  */