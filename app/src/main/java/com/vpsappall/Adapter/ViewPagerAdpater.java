package com.vpsappall.Adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.vpsappall.Fragment.HistoryFragment;
import com.vpsappall.Fragment.HomeLiveFragment;

/**
 * Created by Bilal Khan on 31/5/17.
 */

public class ViewPagerAdpater extends FragmentStatePagerAdapter {


    public ViewPagerAdpater(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        switch (position) {
            case 0:
                frag = new HomeLiveFragment();
                break;
            case 1:
                frag = new HistoryFragment();
                break;

        }
        return frag;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = " ";
        switch (position) {
            case 0:
                title = "Live";
                break;
            case 1:
                title = "History";
                break;
        }

        return title;
    }
}
