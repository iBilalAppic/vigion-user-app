package com.vpsappall.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.vpsappall.Activity.NotificationActivity;
import com.vpsappall.Activity.SuccessActivity;
import com.vpsappall.Database.DBHelper;
import com.vpsappall.Model.NotificationModel;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.vpsappall.Services.RequestGenerator;
import com.vpsappall.Services.ResponseListener;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Bilal Khan on 1/6/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    Context context;
    ArrayList<NotificationModel> arrayList = new ArrayList<>();
    String NotificationId, TrackingId, routeType, userType;
    AppPreferance appPreferance;
    private static ProgressDialog progressDialog1;
    private static int COUNT;

    public NotificationAdapter(NotificationActivity notificationActivity, ArrayList<NotificationModel> arrayList) {

        this.context = notificationActivity;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_notification_adapter, parent, false);
        return new NotificationAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        NotificationId = arrayList.get(position).getNotificationId();
        TrackingId = arrayList.get(position).getTrackingId();
        routeType = arrayList.get(position).getType();
        appPreferance = new AppPreferance();
        userType = appPreferance.getPreferences(context, AppConstant.LoginType);

        holder.packageDetails.setText(arrayList.get(position).getDriverName());
        if (routeType.toString().equals("1") || userType.toString().equals("transporter")) {

            holder.vehcleDetails.setText(arrayList.get(position).getVehicleNumber() + arrayList.get(position).getPackageName());
        } else {
            holder.vehcleDetails.setText(arrayList.get(position).getVehicleNumber() + "  &  " + arrayList.get(position).getPackageName());
        }
        holder.packageStatus.setText(arrayList.get(position).getMessage());
        holder.packageDate.setText(arrayList.get(position).getDate());


        if (arrayList.get(position).getMessage().toString().equals("Package Dispatched")) {
            holder.packageStatus.setTextColor(Color.parseColor("#afaeae"));
        } else if (arrayList.get(position).getMessage().toString().equals("Package Delivered")) {
            holder.packageStatus.setTextColor(Color.parseColor("#27AD5F"));
        } else if (arrayList.get(position).getMessage().toString().equals("Request to End Tracking")) {
            holder.packageStatus.setTextColor(Color.parseColor("#ff0101"));
        } else if (arrayList.get(position).getMessage().toString().equals("")) {
            holder.packageStatus.setVisibility(View.GONE);
        }

        Glide.with(context).load(arrayList.get(position).getDriverPic()).into(holder.profile);

        if (arrayList.get(position).getType().equals("0")) {
            holder.endTracking.setVisibility(View.GONE);
        } else {
            holder.endTracking.setVisibility(View.VISIBLE);
        }




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView profile;
        TextView packageDetails, packageStatus, packageDate, vehcleDetails, endTracking;

        public ViewHolder(View itemView) {
            super(itemView);

            profile = (CircleImageView) itemView.findViewById(R.id.user_Pic);
            vehcleDetails = (TextView) itemView.findViewById(R.id.vehicleNum);
            packageDetails = (TextView) itemView.findViewById(R.id.detailsTxt);
            packageStatus = (TextView) itemView.findViewById(R.id.pacStatusTxt);
            packageDate = (TextView) itemView.findViewById(R.id.dateTxt);
            endTracking = (TextView) itemView.findViewById(R.id.endTracking);

            endTracking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String NotificationId = arrayList.get(getAdapterPosition()).getNotificationId();
                    final String TrackignId = arrayList.get(getAdapterPosition()).getTrackingId();


                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("notificationid", NotificationId);
                        jsonObject.put("trackingid", TrackignId);

                        Log.e("End Track Req", jsonObject.toString());
                        showProgressDialog((Activity) context);
                        RequestGenerator.makePostRequest1(context, AppConstant.EndTracking, jsonObject, true, new ResponseListener() {
                            @Override
                            public void onError(VolleyError error) {
                                cancelProgressDialog();
                                UtilsClass.showToast(context, "Some Problem Occured");
                            }

                            @Override
                            public void onSuccess(String string) throws JSONException {

                                if (!string.equals("")) {
                                    JSONObject jsonObject1 = new JSONObject(string);
                                    String status = jsonObject1.getString("status");
                                    String message = jsonObject1.getString("message");
                                    if (status.equals("success")) {
                                        cancelProgressDialog();
                                        UtilsClass.showToast(context, message);
                                        endTracking.setVisibility(View.GONE);

                                        DBHelper dbHelper=DBHelper.getInstance(context);
                                        dbHelper.deleteDriver(TrackignId);

                                        Log.i(TrackignId+" is exist",""+dbHelper.checkIfValueExists(TrackignId));

                                        Log.i("direRows",""+dbHelper.getNumOfRows());

                                        final NotificationActivity notificationActivity = NotificationActivity.getInstance();

                                        notificationActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                notificationActivity.initUI();

                                            }
                                        });
                                        Intent intent = new Intent(context, SuccessActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        context.startActivity(intent);


                                    } else {
                                        cancelProgressDialog();
                                        UtilsClass.showToast(context, message);
                                    }
                                }

                            }
                        });


                    } catch (JSONException e) {


                    }


                }
            });
        }
    }

    public static void showProgressDialog(final Activity context) {
        try {
            cancelProgressDialog();
            context.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog1 = new ProgressDialog(context);
                    progressDialog1.setMessage("Please wait...");
                    progressDialog1.setCancelable(false);
                    progressDialog1.show();
                    COUNT++;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void cancelProgressDialog() {
        try {
            if (progressDialog1 != null) {
                if (COUNT <= 1) {
                    progressDialog1.cancel();
                    progressDialog1 = null;
                }
                COUNT--;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
