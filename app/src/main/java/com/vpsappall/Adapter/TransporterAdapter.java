package com.vpsappall.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.vpsappall.Activity.LiveTrackingOnMapActivity;
import com.vpsappall.Model.LiveTruckModel;
import com.vpsappall.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Bilal Khan on 31/7/17.
 */

public class TransporterAdapter extends RecyclerView.Adapter<TransporterAdapter.ViewHolder> {

    Context context;
    ArrayList<LiveTruckModel> arrayList = new ArrayList<>();

    public TransporterAdapter(FragmentActivity activity, ArrayList<LiveTruckModel> arrayList) {

        this.context = activity;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_transpporter_card, parent, false);
        return new TransporterAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        LiveTruckModel liveTruckModel = arrayList.get(position);
        Glide.with(context).load(liveTruckModel.getDriverpic()).into(holder.profile);
        holder.destination.setText(liveTruckModel.getDestination());
        holder.driverName.setText(liveTruckModel.getDrivername());
        holder.vehicleNumber.setText(liveTruckModel.getVehiclenumber());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView profile;
        TextView vehicleNumber, driverName, destination;

        public ViewHolder(View itemView) {
            super(itemView);
            profile = (CircleImageView) itemView.findViewById(R.id.driverPicT);
            vehicleNumber = (TextView) itemView.findViewById(R.id.vehicleNumberTxt);
            driverName = (TextView) itemView.findViewById(R.id.driverName);
            destination = (TextView) itemView.findViewById(R.id.destination);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v == itemView) {
                Intent intent = new Intent(context, LiveTrackingOnMapActivity.class);
                intent.putExtra("trackingId", "");
                context.startActivity(intent);
            }

        }
    }
}
