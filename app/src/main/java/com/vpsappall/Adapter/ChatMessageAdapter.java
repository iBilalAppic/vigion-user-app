package com.vpsappall.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vpsappall.Activity.ChatMessageActivity;
import com.vpsappall.Model.MessageModel;
import com.vpsappall.OtherClass.DateTimeUtils;
import com.vpsappall.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Bilal Khan on 2/8/17.
 */

public class ChatMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int MY_MESSAGE = 0, OTHER_MESSAGE = 1, HEADER = 2;
    Context context;
    private ArrayList<MessageModel> messages = new ArrayList<>();

    public ChatMessageAdapter(ChatMessageActivity chatMessageActivity, ArrayList<MessageModel> messages) {
        this.context = chatMessageActivity;
        this.messages = getFormattedList(messages);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case HEADER:
                return new ViewHolderHeader(LayoutInflater.from(context).inflate(R.layout.list_item_chat_header, viewGroup, false));
            case MY_MESSAGE:
                return new ViewHolderItem(LayoutInflater.from(context).inflate(R.layout.item_mine_message, viewGroup, false));
            case OTHER_MESSAGE:
                return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_other_message, viewGroup, false));

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MessageModel item = messages.get(position);
        if (holder.getItemViewType() == MY_MESSAGE) {
            ViewHolderItem mholder = (ViewHolderItem) holder;
            mholder.msg.setText(item.getMessage());
            mholder.textTime.setText(DateTimeUtils.formateDate(item.getDate()));
//            if(!item.getProfilepic().isEmpty()){
//                Picasso.with(context).load(item.getProfilepic()).into(mholder.imageView);
//            }
            if (item.getSendingvisivility() != null) {
                mholder.textsending.setVisibility(View.VISIBLE);
            } else {
                mholder.textsending.setVisibility(View.GONE);
            }

        } else if (holder.getItemViewType() == OTHER_MESSAGE) {
            ViewHolder mholder = (ViewHolder) holder;
            mholder.msg.setText(item.getMessage());
            mholder.textTime.setText(DateTimeUtils.formateDate(item.getDate()));
            mholder.txt_Name.setText(item.getUsername());
            if (!item.getProfilepic().isEmpty()) {
                Picasso.with(context).load(item.getProfilepic()).resize(100, 100).centerCrop().into(mholder.imageView);
            }

        } else if (holder.getItemViewType() == HEADER) {
            ViewHolderHeader mholder = (ViewHolderHeader) holder;
            mholder.msg.setText(item.getDate());
        }
    }

    @Override
    public int getItemViewType(int position) {
        MessageModel item = messages.get(position);
        if (item.getReceiverId() == null) {
            return HEADER;
        } else {
            String receiverType = item.getType();
            switch (receiverType) {
                case "sender":
                    return OTHER_MESSAGE;
                case "receiver":
                    return MY_MESSAGE;
                default:
                    return 0;
            }
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    private ArrayList<MessageModel> getFormattedList(ArrayList<MessageModel> dataSet) {
        Date prevDate = null;
        ArrayList<MessageModel> messages = new ArrayList<>();
        for (MessageModel message : dataSet) {
            Date currentDate = DateTimeUtils.getDateFromString(message.getDate(), "dd/MM/yyyy");
            if (prevDate == null || !currentDate.equals(prevDate)) {
                MessageModel message1 = new MessageModel();
                message1.setDate(DateTimeUtils.getStringFromDate(currentDate, "dd MMM,yyyy"));
                messages.add(message1);
            }
            prevDate = currentDate;
            messages.add(message);
        }
        return messages;
    }

    public void addItemsToList(ArrayList<MessageModel> message) {
        messages.clear();
        messages.addAll(getFormattedList(message));
    }

    public void addsendmsg(MessageModel message) {
//        messages.add(message);

        Date prevDate = null;
        Date currentDate = DateTimeUtils.getDateFromString(message.getDate(), "dd/MM/yyyy");
        if (prevDate == null || !currentDate.equals(prevDate)) {
            MessageModel message1 = new MessageModel();
            message1.setDate(DateTimeUtils.getStringFromDate(currentDate, "dd MMM,yyyy"));

        }
        prevDate = currentDate;
        messages.add(message);

//        this.notifyDataSetChanged();

    }

    public static class ViewHolderItem extends RecyclerView.ViewHolder {
        TextView msg;
        ImageView imageView;
        TextView textTime, textsending;

        public ViewHolderItem(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            msg = (TextView) v.findViewById(R.id.text);
            imageView = (ImageView) v.findViewById(R.id.ivUserPics);
            textTime = (TextView) v.findViewById(R.id.textTime);
            textsending = (TextView) v.findViewById(R.id.textsending);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView msg;
        ImageView imageView;
        TextView txt_Name, textTime;

        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            msg = (TextView) v.findViewById(R.id.text);
            imageView = (ImageView) v.findViewById(R.id.ivUserPics);
            textTime = (TextView) v.findViewById(R.id.textTime);
            txt_Name = (TextView) v.findViewById(R.id.txt_Name);
        }

    }

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView msg;


        public ViewHolderHeader(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            msg = (TextView) v.findViewById(R.id.text);
        }
    }
}
