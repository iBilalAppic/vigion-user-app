package com.vpsappall.ChatService;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.vpsappall.Activity.ChatMessageActivity;
import com.vpsappall.CallBack.ChatCallBack;
import com.vpsappall.OtherClass.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Bilal Khan on 2/8/17.
 */
public class SendMessageWebservice extends AsyncTask<String, Void, String> {

    Context context;
    JSONObject requestType;
    ChatCallBack chatCallBack;
    String jsonstring = null;
    int pos;
    boolean refresh = false;


    public SendMessageWebservice(ChatMessageActivity chatActivity, JSONObject jsonObject, ChatCallBack chatCallBack) {
        this.context = chatActivity;
        this.requestType = jsonObject;
        this.chatCallBack = chatCallBack;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... params) {

        try {
            URL urlobject = new URL(AppConstant.SendMessage);
            HttpURLConnection con = (HttpURLConnection) urlobject.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

//            String Strpost = "{\"cuiId\":\"" + requestType + "\"}";
            String Strpost = requestType.toString();

            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(Strpost.toString());
            wr.flush();

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            con.connect();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
            }
            con.disconnect();
            Log.v("Response", sb.toString());
            jsonstring = sb.toString();

            Log.v("JsonString", jsonstring);
            if (!jsonstring.equals("")) {

                try {

                    JSONObject jsonObject = new JSONObject(jsonstring);
                    String status = jsonObject.getString("status");
                    if (status.equals("success")) {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {

        chatCallBack.calledFromMain(jsonstring, refresh);

        super.onPostExecute(s);
    }
}
