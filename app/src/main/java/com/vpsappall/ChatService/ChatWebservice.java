package com.vpsappall.ChatService;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.vpsappall.Activity.ChatMessageActivity;
import com.vpsappall.CallBack.ChatCallBack;
import com.vpsappall.OtherClass.AppConstant;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Bilal Khan on 2/8/17.
 */

public class ChatWebservice extends AsyncTask<String, Void, String> {
    Context context;
    JSONObject requestType;
    ChatCallBack chatCallBack;
    String jsonstring = null;
    int pos;
    boolean refresh = false;
    boolean isfirstrun = false;
    ProgressDialog pd;


    public ChatWebservice(ChatMessageActivity context, JSONObject jsonObject, ChatCallBack chatCallBack, boolean refresh, boolean isfirstrun) {
        this.context = context;
        this.requestType = jsonObject;
        this.chatCallBack = chatCallBack;
        this.refresh = refresh;
        this.isfirstrun = isfirstrun;
        pd = new ProgressDialog(context);

    }


    @Override
    protected void onPreExecute() {
        if (isfirstrun) {
            pd.setMessage("loading");
            pd.show();
        } else {

        }
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... params) {

        try {
            URL urlobject = new URL(AppConstant.HistoryChat);
            HttpURLConnection con = (HttpURLConnection) urlobject.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

//            String Strpost = "{\"cuiId\":\"" + requestType + "\"}";
            String Strpost = requestType.toString();

            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(Strpost.toString());
            wr.flush();

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            con.connect();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
            }
            con.disconnect();
            Log.v("Response", sb.toString());
            jsonstring = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if (isfirstrun) {
            pd.dismiss();
        } else {

        }
        chatCallBack.calledFromMain(jsonstring, refresh);

        super.onPostExecute(s);
    }

}