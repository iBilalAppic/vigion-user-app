package com.vpsappall.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.vpsappall.OtherClass.UtilsClass;
import com.vpsappall.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * Created by Bilal Khan on 2/6/17.
 */

public class LocationUpdateService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Context mContext;

    private int PERMISSION_REQUEST_CODE = 23;
    private GoogleApiClient mGoogleApiClient;
    private int GPS_DISABLE_NOTICATION_ID = 0x132432;
    Location mLastLocation;
    LocationRequest mLocationRequest;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        UtilsClass.showLog("Service Started");
        mContext = this;
        if (UtilsClass.isGpsEnabled(mContext)) {
            basicVariablesInit();
        } else {
            this.stopSelf();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        UtilsClass.showLog("Service Stopped");
        stopLocationUpdates();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    private void basicVariablesInit() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        checkLocationSettings();
    }

    private void checkLocationSettings() {
        if (!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();


        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        final PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        showNotification(getString(R.string.enable_gps_notification_message), GPS_DISABLE_NOTICATION_ID);
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        showNotification(getString(R.string.enable_gps_notification_message), GPS_DISABLE_NOTICATION_ID);
                        break;
                }
            }
        });
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient != null) {

            if (mGoogleApiClient.isConnected()) {

                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient,
                        this
                ).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
                mGoogleApiClient.disconnect();
            }
        }
    }


    @SuppressWarnings("MissingPermission")
    protected void startLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {

                }
            });

        }
    }

    @SuppressWarnings("MissingPermission")
    private void getLocation(Location location) {
        if (location != null) {

//            Double sLat = location.getLatitude();
//            Double sLong = location.getLongitude();
            String sLat = String.valueOf(location.getLatitude());
            String sLong = String.valueOf(location.getLongitude());

            UtilsClass.showLog(sLat + "," + sLong);
//            if (Utils.isUserLoggedIn(mContext))
//                sendLocationToServer(sLat, sLong);

        }
    }

    private void sendLocationToServer(final String lat, final String lng) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null) {
//
//

        } else {
            UtilsClass.showToast(mContext, getString(R.string.internet_not_connected));
        }
    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        getLocation(mLastLocation);
        UtilsClass.showLog("GoogleApi connection connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        UtilsClass.showLog("GoogleApi connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        UtilsClass.showLog("GoogleApi connection failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        getLocation(location);
    }
}
