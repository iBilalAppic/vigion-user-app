package com.vpsappall.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.vpsappall.Activity.LiveTrackingOnMapActivity;
import com.vpsappall.OtherClass.UtilsClass;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Bilal Khan on 21/6/17.
 */

public class TrackingService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                final LiveTrackingOnMapActivity activity = LiveTrackingOnMapActivity.getInstance();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.showLiveRoute();
                        }
                    });
                }
            }
        };
        timer.schedule(task, 0, 5000);

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        UtilsClass.showLog("Track Service Stopped");
    }
}
