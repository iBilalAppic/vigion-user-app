package com.vpsappall.Services;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.vpsappall.Activity.MainActivity;
import com.vpsappall.Activity.NotificationActivity;
import com.vpsappall.Fragment.HomeLiveFragment;
import com.vpsappall.Fragment.TransporterLiveFragment;
import com.vpsappall.OtherClass.AppConstant;
import com.vpsappall.OtherClass.AppPreferance;
import com.vpsappall.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Calendar;

/**
 * Created by Bilal Khan on 16/6/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String TAG = "Vigion App";
    private String newMessage;
    private AppPreferance appPreferance;
    Context context;


    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        String refreshedToken = s;
        context = MyFirebaseMessagingService.this;
        appPreferance = new AppPreferance();
        appPreferance.setPreferences(context, AppConstant.deviceToken, refreshedToken);
        Log.e("refreshedToken", refreshedToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        String message = "";
        String name = "";
        String number = "";
        String serverID = "";
        String imageUrl = "";

        try {
            message = remoteMessage.getData().get(AppConstant.notification_key_message);
            name = remoteMessage.getData().get(AppConstant.notification_key_name);
            imageUrl = remoteMessage.getData().get(AppConstant.notification_key_image_url);
        } catch (Exception e) {
            message = "Testing";
        }

        if (newMessage == null) {
            newMessage = message;
        }

        MainActivity base = MainActivity.getInstance();

        final HomeLiveFragment fragment = HomeLiveFragment.getInstance();
        final TransporterLiveFragment fragmentTrans = TransporterLiveFragment.getInstance();
        final NotificationActivity fragment1 = NotificationActivity.getInstance();

        if (fragment != null) {
            Activity activity = fragment.getActivity();
            if (activity != null) {
                fragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fragment.initUI();

                    }
                });
            }
        }

        if (fragmentTrans != null) {
            Activity activity = fragmentTrans.getActivity();
            if (activity != null) {
                fragmentTrans.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fragmentTrans.initUI();

                    }
                });
            }
        }

        if (fragment1 != null) {
            fragment1.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    fragment1.initUI();

                }
            });
        }

        appPreferance = new AppPreferance();
        String notiPref = appPreferance.getPreferences(getApplicationContext(), AppConstant.getNoti);
        if (notiPref.toString().equals("1")) {
            sendNotification(new Notification(name, number, newMessage, imageUrl));
        } else {
        }


    }


    private void sendNotification(Notification messageBody) {

        Class classtoLoad = null;
        boolean addContact = false;
        classtoLoad = MainActivity.class;

        Intent intent = new Intent(this, classtoLoad);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri;
        if (appPreferance.getPreferences(getApplicationContext(), AppConstant.defaultRingtone).equals("")) {
            defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        } else {
            defaultSoundUri = Uri.parse(appPreferance.getPreferences(getApplicationContext(), AppConstant.defaultRingtone));
        }

// this is a my insertion looking for a solution
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_new_logo)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_new_logo))
                .setContentTitle(TAG)
                .setContentText(messageBody.getMessage())
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody.getMessage()))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationID = (int) ((Calendar.getInstance().getTime().getTime() / 1000L) % Integer.MAX_VALUE);

        notificationManager.notify(notificationID, notificationBuilder.build());
    }


    public class Notification {
        String name, number, message, serverID, imageUrl;

        public Notification(String name, String number, String message, String imageUrl) {
            this.name = name;
            this.number = number;
            this.message = message;
            this.imageUrl = imageUrl;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getServerID() {
            return serverID;
        }

        public void setServerID(String serverID) {
            this.serverID = serverID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
